<?php
session_start();
if(isset($_SESSION["id_cliente"]))
{
	require_once ('db.class.php');
	include('config_connection.php');
	header("Content-Type: application/json; charset=UTF-8");
	if(isset($_GET["clientId"]) && !isset($_GET["idNotifica"]) && !isset($_GET["eliminaNotifica"]))
	{
		$id_cliente = (int) htmlentities($_GET["clientId"], ENT_QUOTES);
		if(is_int($id_cliente))
		{
			$sql = "SELECT * FROM notifiche WHERE cod_cliente = $id_cliente ORDER BY data asc;";
			
			$ricevuta = $db->Query($sql);
			$rows = array();
			while ($row = mysqli_fetch_assoc($ricevuta)) 
			{
				$rows[] = $row;
			}
			echo(json_encode($rows));
		}
	}
	else if(isset($_GET["idNotifica"]) && isset($_GET["clientId"]))
	{
		$id_notifica = (int) htmlentities($_GET["idNotifica"], ENT_QUOTES);
		if(is_int($id_notifica))
		{
			$sql = "UPDATE notifiche SET letto = 1 WHERE id = $id_notifica;";
			$ricevuta = $db->Query($sql);
		}
		$id_cliente = (int) htmlentities($_GET["clientId"], ENT_QUOTES);
		if(is_int($id_cliente))
		{
			$sql = "SELECT * FROM notifiche WHERE cod_cliente = $id_cliente ORDER BY data asc;";
			
			$ricevuta = $db->Query($sql);
			$rows = array();
			while ($row = mysqli_fetch_assoc($ricevuta)) 
			{
				$rows[] = $row;
			}
			echo(json_encode($rows));
		}
	}
	else if(isset($_GET["eliminaNotifica"]) && isset($_GET["clientId"]))
	{
		$id_notifica = (int) htmlentities($_GET["eliminaNotifica"], ENT_QUOTES);
		if(is_int($id_notifica))
		{
			$sql = "DELETE FROM notifiche WHERE id = $id_notifica;";
			$ricevuta = $db->Query($sql);
		}
		$id_cliente = (int) htmlentities($_GET["clientId"], ENT_QUOTES);
		if(is_int($id_cliente))
		{
			$sql = "SELECT * FROM notifiche WHERE cod_cliente = $id_cliente ORDER BY data asc;";
			
			$ricevuta = $db->Query($sql);
			$rows = array();
			while ($row = mysqli_fetch_assoc($ricevuta)) 
			{
				$rows[] = $row;
			}
			echo(json_encode($rows));
		}
	}
	else
	{
		echo("null");
	}
}
else
{
	header("location:index.php");
}
?>