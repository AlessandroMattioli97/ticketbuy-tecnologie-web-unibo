<div class="carousel">
	<div class="carousel slide" data-ride="carousel">
  		<div class="carousel-inner">
			<?php
			$sql = 'SELECT e.id as id, se.nome_breve as nome_breve, de.citta as citta, e.immagine, e.carousel FROM (`evento` as e inner join data_evento as de on e.id = de.id_evento) INNER JOIN soggetto_evento as se on se.id = e.id_soggetto WHERE de.posti_rimanenti > 0 AND e.carousel is not null GROUP BY e.id ORDER BY MIN(de.data) ASC;';
			$CAROUSEL = $db->GetRowsAsoc($sql);
			$count = 0;
			foreach($CAROUSEL as $evento)
			{
				if($count == 0)
				{
					echo'<div style="background-image:url(' . $evento["carousel"] . ')" class="carousel-item active">
							<div class="background-descrizione">
								<div class="descrizione">
									<h3>' . $evento["nome_breve"] . '</h3>
									<h6>' . $evento["citta"] . '</h6>
									<a class="biglietti-btn" href="./scheda-evento.php?id=' . $evento["id"] . '">BIGLIETTI</a>
								</div>
							</div>
						</div>';
				}
				else
				{
					echo'<div style="background-image:url(' . $evento["carousel"] . ')" class="carousel-item">
							<div class="background-descrizione">
								<div class="descrizione">
									<h3>' . $evento["nome_breve"] . '</h3>
									<h6>' . $evento["citta"] . '</h6>
									<a class="biglietti-btn" href="./scheda-evento.php?id=' . $evento["id"] . '">BIGLIETTI</a>
								</div>
							</div>
						</div>';
				}
				$count++;
			}
			?>
		</div>
	</div>
</div>