<?php
	session_start();
	require_once ('db.class.php');
	include('config_connection.php');
	$_SESSION['errore_login'] = "";
	if(isset($_POST['email']) && isset($_POST['password']))
	{
		$email = htmlentities($_POST["email"], ENT_QUOTES);
		$password = md5(htmlentities($_POST["password"], ENT_QUOTES));
		$NumeroRighe = 0;

		$sql = "select * from clienti where email='$email' and password='$password'";
		$NumeroRighe = $db->NumRows($sql);

		if ($NumeroRighe == 1) 
		{
			$sql                   = "select id, nome, cognome from clienti where email='$email' and password='$password' LIMIT 1";
			$Temp                  = $db->GetRowsAsoc($sql)[0];
			$_SESSION['id_cliente']        = $Temp["id"];
			$_SESSION["cliente"] = $Temp["nome"] . " " . $Temp["cognome"];
			$db->CloseConnection();
			if(isset($_GET["root"]) && $_GET["root"] != "")
			{
				if($_GET["root"] == "scheda-evento")
				{
					$id_evento = htmlentities($_GET["id"], ENT_QUOTES);
					header("location:scheda-evento.php?id=$id_evento");
				}
				if($_GET["root"] == "carrello")
				{
					$id_cliente = $_SESSION['id_cliente'];
					header("location:cart.php?id=$id_cliente");
				}
				
			}
			else
			{
				header("location:index.php");
			}
		}
		else if ($NumeroRighe != 1) 
		{
			$_SESSION['errore_login'] = '<p class="error">Username o password errati</p>';
			$db->CloseConnection();
		}
	}
?>

<html lang="it">
	<head>
		<link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,700&display=swap" rel="stylesheet">
		<script src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/min.jcarousel.js"></script>
		<script src="./js/bootstrap.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet"  href="css/bootstrap.min.css">
		<meta charset="UTF-8">
		<title>Login | OneTicket</title>
		<style>
			body{background-image: url(img/background.jpg);}
			.mask{background-color: #fff; max-width: 750px; padding: 80px; padding-bottom: 20px; padding-top: 0px; margin: 0px auto; margin-top: 18%; border: 4px solid #d66c0f; border-radius: 20px;}
			.login-btn{ background-color: rgb(0, 82, 204); color: #fff; height: 60px;}
			.logo{ max-width: 200px;}
			.signup{ font-size: 18px; text-align: center; padding-top: 15px;}
			a:hover{ text-decoration: none;}
			.background{background-color:rgba(214, 108, 15, 0.5); height: 100%; position: absolute; width: 100%;}
			.error{ text-align: center; font-size: 20px; color: f00;}
		</style>
	</head>
	<body>
		<div class="background">
			<div class="container">
				<div class="mask">
					<a href="index.php"><img src="img/logo.png" alt="ticket buy logo" class="img-fluid logo mx-auto d-block my-3 pt-3"></a>
					<form method="post" action="#">
						<div class="form-group">
							<label for="email"><strong>Email</strong></label>
							<input class="form-control" type="text" name="email" id="email" placeholder="Inserisci la tua email..." required>
						</div>
						<div class="form-group">
							<label for="password"><strong>Password</strong></label>
							<input class="form-control" type="password" name="password" id="password" placeholder="Inserisci la tua email..." required>
						</div>
						<div class="form-group">
							<input class="form-control login-btn" type="submit" value="LOGIN">
						</div>
					</form>
					<p class="signup">Non hai un account? <a href="signin.php"><span>Registrati!</span></a></p>
					<?php echo $_SESSION['errore_login']; ?>
				</div>
			</div>
		</div>
	</body>
</html>