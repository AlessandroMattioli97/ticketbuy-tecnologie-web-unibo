<h3 class="subtitle">ULTIMI BIGLIETTI DISPONIBILI</h3>
<div class="jcarousel-wrapper">
    <div class="jcarousel" data-jcarousel="true">
    	 <ul>
           <?php
			$sql = 'SELECT e.id as id, se.nome_breve as nome_breve, MONTH(MIN(de.data)) as mese, DAY(MIN(de.data)) as giorno, de.citta as citta, e.immagine FROM (`evento` as e inner join data_evento as de on e.id = de.id_evento) INNER JOIN soggetto_evento as se on se.id = e.id_soggetto WHERE de.posti_rimanenti > 0 GROUP BY e.id ORDER BY de.posti_rimanenti ASC;';
			$EVENTI = $db->GetRowsAsoc($sql);
			$mesi = array(
				1 => "GEN",
				2 => "FEB",
				3 => "MAR",
				4 => "APR",
				5 => "MAG",
				6 => "GIU",
				7 => "LUG",
				8 => "AGO",
				9 => "SET",
				10 => "OTT",
				11 => "NOV",
				12 => "DIC",
			);
			foreach($EVENTI as $evento)
			{
				echo'<li>
					<div class="event-gallery">
						<p class="event-gallery-name">' . $evento["nome_breve"] . '</p>
						<a href="./scheda-evento.php?id=' . $evento["id"] . '"><img src="' . $evento["immagine"] . '" alt="Immagine ' . $evento["nome_breve"] . '"></a>
						<span class="event-gallery-first-date">' . $mesi[$evento["mese"]] . '<br>' . $evento["giorno"] . '</span>
						<span class="event-gallery-city">' . $evento["citta"] . '</span>
					</div>
				</li>';
			}
			?>
        </ul>
    </div>
    <a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true">‹</a>
	<a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true">›</a>
</div>