<?php
    include("./header.php");
    $myid=$_SESSION["id_cliente"];
    $sql="SELECT * FROM clienti WHERE id = '$myid'";
    $result = $db->GetRowsAsoc($sql)[0];
	$result2 = 1;
	if(isset($_POST["Nome"]) && $_POST["Nome"] != "")
	{
		$nome = htmlentities($_POST["Nome"], ENT_QUOTES);
		$cognome = htmlentities($_POST["Cognome"], ENT_QUOTES);
		$email = htmlentities($_POST["Email"], ENT_QUOTES);
		$data = htmlentities($_POST["Data"], ENT_QUOTES);
		$cellulare = htmlentities($_POST["Cellulare"], ENT_QUOTES);
		$sql = "UPDATE clienti SET `nome` = '$nome', `cognome` = '$cognome', `email` = '$email', `data_nascita` = '$data', `cellulare` = '$cellulare' WHERE id = $myid;";
		$result1 = $db->Query($sql);
		if(isset($_POST["Password"]) && $_POST["Password"] != "")
		{
			$password = md5(htmlentities($_POST["Password"], ENT_QUOTES));
			$sql = "UPDATE clienti SET `password` = '$password' WHERE id = $myid;";
			$result2 = $db->Query($sql);
		}
		if($result1 == 1 && $result2 == 1)
		{
			$_SESSION["stato_operazione"] = '<p class="success">Dati modificati con successo!!</p>';
		}
		else
		{
			$_SESSION["stato_operazione"] = '<p class="error">Dati NON modificati, controllare i dati inseriti!!</p>';
		}
		header("location:riservata.php");
	}
?>

<div class="container">
				<p class="padding margin-left-min black title-content subtitle pb-3"> Modifica i tuoi dati >></p>
				<div class="padding">
					<form method="post" action="#">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name">Nome</label>
									<input type="text" id="name" value="<?php echo $result["nome"]; ?>" class="form-control" name="Nome" required/>
								</div>
								<div class="form-group">
									<label for="cognome">Cognome</label>
									<input type="text" id="cognome" value="<?php echo $result["cognome"]; ?>" class="form-control" name="Cognome" required/>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="text" id="email" value="<?php echo $result["email"]; ?>" class="form-control" name="Email" required/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="data_nascita">Data di nascita</label>
									<input type="date" id="data_nascita" value="<?php echo $result["data_nascita"]; ?>" class="form-control" name="Data" required/>
								</div>
                                <div class="form-group">
									<label for="cellulare">Cellulare</label>
									<input type="text" id="cellulare" value="<?php echo $result["cellulare"]; ?>" class="form-control" name="Cellulare" required>
								</div>
								<div class="form-group">
									<label for="password">Password</label>
									<input type="password" id="password" class="form-control" name="Password" />
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label for="annulla">Annulla</label>
											<a href="riservata.php"><input type="button" class="form-control btn btn-primary" id="annulla" value="Annulla"></a>
										</div>
										<div class="col-md-6">
											<label for="invia">Invia Dati</label>
											<input type="submit" class="form-control btn btn-primary" value="Aggiorna Dati">
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
<?php include("./footer.php") ?>