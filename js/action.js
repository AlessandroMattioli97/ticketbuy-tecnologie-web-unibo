// JavaScript Document
var datag = [];
$(document).ready(function(){

	(function($) {
    $(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 1920) {
                    width = width /5;
                }else if (width >= 1150) {
                    width = width / 4;
                }else if (width >= 850) {
                    width = width / 3;
                } else if (width >= 550) {
                    width = width / 2;
                }else if (width < 549) {
                    width = width / 1;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);
	
$(".minus").click(function (e){
	var id_evento;
	var quantita;
	var link;
	id_evento = $(this).attr("data-id");
	quantita = document.getElementById("q-" + id_evento).innerHTML;
	if(quantita > 1)
	{
		quantita--;
	}
	document.getElementById("q-" + id_evento).innerHTML = quantita;
	link = document.getElementById("link-" + id_evento).getAttribute("href");
	link = link.substring(0,link.length - 1);
	link = "" + link + quantita;
	document.getElementById("link-" + id_evento).setAttribute("href", link);
});
	
$(".plus").click(function (e){
	var id_evento;
	var quantita;
	var link;
	id_evento = $(this).attr("data-id");
	quantita = document.getElementById("q-" + id_evento).innerHTML;
	if(quantita < 9)
	{
		quantita++;
	}
	document.getElementById("q-" + id_evento).innerHTML = quantita;
	link = document.getElementById("link-" + id_evento).getAttribute("href");
	link = link.substring(0,link.length - 1);
	link = "" + link + quantita;
	document.getElementById("link-" + id_evento).setAttribute("href", link);
});
	
$('.ajaxNotifiche').click(function(e){
	e.preventDefault();
	var url = $(e.currentTarget).attr('href');
	$.ajax({
  		url: url,
  		dataType: "json",
  		success: logResultsNotification
	});
});
	
$(document).on('click','.segna-letto',function(e){
	e.preventDefault();
	var url = $(e.currentTarget).attr('href');
	$.ajax({
  		url: url,
  		dataType: "json",
		success: logResultsNotification
	});
});
	
$(document).on('click','.elimina',function(e){
	e.preventDefault();
	var url = $(e.currentTarget).attr('href');
	$.ajax({
  		url: url,
  		dataType: "json",
		success: logResultsNotification
	});
});
	
function logResultsNotification(data)
{
	$(".table-body-notification").html('');
	$.each(data, function(i, item) {
		if(item.letto == 0)
		{
			$(".table-body-notification").append(
			'<tr>' + 
				'<td class="td-descrizione-notifica">' + item.descrizione + '</td>' +
				'<td class="td-data-notifica">' + item.data + '</td>' +
				'<td><a class="segna-letto" href="ajax.php?idNotifica=' + item.id + '&clientId=' + item.cod_cliente + '"><span class="glyphpro glyphpro-eye_open white"></span></a></td>' +
				'<td><a class="elimina" href="ajax.php?eliminaNotifica=' + item.id + '&clientId=' + item.cod_cliente + '"><span class="glyphpro glyphpro-bin white"></span></a></td>' +
			'</tr>'
			); 	
		}
		else
		{
			$(".table-body-notification").append(
			'<tr>' + 
				'<td class="td-descrizione-notifica">' + item.descrizione + '</td>' +
				'<td class="td-data-notifica">' + item.data + '</td>' +
				'<td>Letta</td>' +
				'<td><a class="elimina" href="ajax.php?eliminaNotifica=' + item.id + '&clientId=' + item.cod_cliente + '"><span class="glyphpro glyphpro-bin white"></span></a></td>' +
			'</tr>'
		); 
		}
		
	});
}
	
});