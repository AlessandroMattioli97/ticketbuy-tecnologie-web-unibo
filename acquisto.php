<?php 
include("./header.php"); 
if(isset($_POST["numeri-carta"]))
{
	$id_cliente = $_SESSION["id_cliente"];
	$numero_carta = htmlentities($_POST["numeri-carta"], ENT_QUOTES);
	$scadenza_carta = htmlentities($_POST["scadenza-carta"], ENT_QUOTES);
	$cvv_carta = htmlentities($_POST["cvv-carta"], ENT_QUOTES);
	$nome_carta = htmlentities($_POST["nome-carta"], ENT_QUOTES);
	if(strlen($numero_carta) == 16 && strlen($scadenza_carta) == 4 && strlen($cvv_carta) == 3 && $nome_carta != "")
	{
		$sql="SELECT c.id as id, de.id as id_evento, c.quantita, de.prezzo, de.data, de.citta, e.nome_evento FROM ((data_evento as de inner join carrello as c on c.id_data_evento = de.id) inner join evento as e on e.id = de.id_evento) WHERE c.id_cliente = '$id_cliente';";
		$CARRELLO = $db->GetRowsAsoc($sql);
					
		$sql = "SELECT SUM(de.prezzo * c.quantita) as totale FROM data_evento as de inner join carrello as c on c.id_data_evento = de.id WHERE id_cliente = $id_cliente;";
		$subtotale = $db->GetRowsAsoc($sql)[0]["totale"];
		
		foreach($CARRELLO as $articolo)
		{
			$sql = "INSERT INTO `ordini` (`descrizione`, `stato`, `data_ordine`, `quantita`, `prezzo_totale`, `id_cliente`, `id_data_evento`) VALUES ('" . htmlentities($articolo["nome_evento"], ENT_QUOTES) . " - " . $articolo["citta"] . " - " . $articolo["data"] . "', 'In Programma', '" . date("y-m-d") . "', '" . $articolo["quantita"] . "', '" . $articolo["quantita"] * $articolo["prezzo"] . "', '" . $_SESSION["id_cliente"] . "', '" . $articolo["id_evento"] . "');";
			$db->Query($sql);
			echo $sql . "<br>";
		}
		
		$sql = "DELETE FROM carrello WHERE id_cliente = $id_cliente;";
		$db->Query($sql);
		
		$_SESSION["stato_operazione"] = '<p class="success">Ordine Effettuato con Successo!!</p>';
		
		//header("location:riservata.php?id=$id_cliente");
	}
	else
	{
		$_SESSION["stato_operazione"] = '<p class="error">Ordine non Effettuato, dati della carta errati!!</p>';
	}
}
?>
<div class="container">
	<?php echo $_SESSION["stato_operazione"]; ?>
	<h2 class="pb-5 pt-3 subtitle">Pagamento Ordine</h2>
	<form method="post" action="#">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label for="numero_carta">Inserisci il numero della carta *</label>
					<input type='text' onkeypress='validate(event)' name="numeri-carta" class="form-control" maxlength="16" id="numero_carta" required />
				</div>
				<div class="form-group">
					<label for="scadenza_carta">Inserisci scadenza Carta (MM/AA) *</label>
					<input type="text" onkeypress='validate(event)' name="scadenza-carta" class="form-control" maxlength="4" id="scadenza_carta" required />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="cvv">Inserisci il CVV (si trova sul retro della carta) *</label>
					<input type="text" onkeypress='validate(event)' name="cvv-carta" class="form-control" maxlength="3" id="cvv" required />
				</div>
				<div class="form-group">
					<label for="intestatario">Inserisci l'intestatario della carta *</label>
					<input type="text" name="nome-carta" class="form-control" id="intestatario">
				</div>
				<div class="form-group">
					<input value="Procedi all'acquisto" type="submit" class="wrap-button-attivita-rapida__btn mt-3" required />
				</div>
			</div>
		</div>
	</form>
</div>
<?php include("./eventi-in-tendenza.php") ?>
<script>
	function validate(evt) {
  var theEvent = evt || window.event;
  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
</script>
<?php 
	include("./footer.php");
	$_SESSION["stato_operazione"] = "";
?>