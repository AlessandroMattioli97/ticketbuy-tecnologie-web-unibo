<?php
include("./notify_subr.php");
//Sopprimo i warning php
error_reporting(E_ERROR | E_PARSE);
// Includo la classe
require_once ('db.class.php');
include('config_connection.php');
// Creo l'oggetto database
$_SESSION["current_page"] = $_SERVER['REQUEST_URI'];
?>
<html lang="it">
<head>
	<link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,700&display=swap" rel="stylesheet">
	<script src="./js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="./js/min.jcarousel.js"></script>
	<script src="./js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet"  href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css?v1.2">
	<link rel="stylesheet" href="css/glyphicons.css">
	<meta charset="UTF-8">
	<title>One Ticket - Vendita di biglietti di Musica, Cultura, Eventi e molto altro...</title>
</head>

<body>
	<header class="intestazione">
		<div class="container-fluid">
			<div class="row justify-content-between">
				<div class="col-lg-2 col-xs-3">
					<a href="index.php"><img src="img/logo.png" alt="ticket buy logo" class="img-fluid logo mx-auto d-block"></a>
				</div>
				<div class="col-lg-7">
					<form method="get" action="./ricerca.php">
						<div class="form-group">
							<input type="text" name="ricerca" class="form-control search-header" id="ricerca" placeholder="Cerca per nome evento, citta o artista...">
						</div>
					</form>
				</div>
				<?php
					if(isset($_SESSION["id_cliente"])){ ?>
				<div class="col-lg-3 col-xs-3">
					<div class="row justify-content-center">
						<div class="col-xs-6">
							<a href="riservata.php"><p>Benvenuto <?php echo $_SESSION["cliente"]; ?></p></a>
						</div>
						<div class="col-xs-6">
							<a href="cart.php"><span class="glyphpro glyphpro-shopping_cart"></span></a>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-xs-12">
							<a href="logout.php"><p class="logout">ESCI <span class="glyphpro glyphpro-log_out"></span></p></a>
						</div>
					</div>
				</div>

				<?php }else
						{ ?>

				<div class="col-lg-3 col-xs-3">
					<div class="row justify-content-center">
						<div class="col-xs-6">
							<a href="login.php"><img class="login-logo" alt="accedi area personale" src="img/login.png"></a>
						</div>
						<div class="col-xs-6">
							<a href="login.php"><p>Login / Registrati</p></a>
						</div>
					</div>
				</div>
				  <?php } ?>
			</div>
		</div>
	</header>
	<header id="menu">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-dark">
				<a class="navbar-brand" href="#">Menu</a>
				<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="navbar-collapse collapse" id="navbarColor01" style="">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="index.php">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="categoria-evento.php?id=1">Musica</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="categoria-evento.php?id=2">Festival</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="categoria-evento.php?id=3">Arte</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="categoria-evento.php?id=4">Teatro</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="categoria-evento.php?id=5">Tempo Libero</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
<?php 
	$_SESSION["errore_login"] = "";
	$_SESSION["not"] = "";
?>