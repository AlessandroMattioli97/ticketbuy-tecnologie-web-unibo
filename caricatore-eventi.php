<div class="container-fluid">
	<div class="events">
		<div class="row">
			<?php
				$categoria = htmlentities($_GET["id"], ENT_QUOTES);
				switch($categoria)
				{
					case 1:
						{
							$categoria = "Musica";
							break;
						}
					case 2:
						{
							$categoria = "Festival";
							break;
						}
					case 3:
						{
							$categoria = "Arte";
							break;
						}
					case 4:
						{
							$categoria = "Teatro";
							break;
						}
					case 5:
						{
							$categoria = "Tempo Libero";
							break;
						}
				}
				if(isset($_GET["ricerca"]))
				{
					$ricerca = htmlentities($_GET["ricerca"], ENT_QUOTES);
					$sql = 'SELECT e.id as id, se.nome_breve as nome_breve, MONTH(MIN(de.data)) as mese, DAY(MIN(de.data)) as giorno, de.citta as citta, e.immagine FROM (`evento` as e inner join data_evento as de on e.id = de.id_evento) INNER JOIN soggetto_evento as se on se.id = e.id_soggetto WHERE (se.nome_breve LIKE "%' . $ricerca . '%" or de.citta LIKE "%' . $ricerca . '%") and de.posti_rimanenti > 0 GROUP BY e.id ORDER BY MIN(de.data) ASC;';
				}
				else
				{
					$sql = 'SELECT e.id as id, se.nome_breve as nome_breve, MONTH(MIN(de.data)) as mese, DAY(MIN(de.data)) as giorno, de.citta as citta, e.immagine FROM (`evento` as e inner join data_evento as de on e.id = de.id_evento) INNER JOIN soggetto_evento as se on se.id = e.id_soggetto WHERE e.tipologia_evento = "' . $categoria . '" and de.posti_rimanenti > 0 GROUP BY e.id ORDER BY MIN(de.data) ASC;';
				}
				$EVENTI = $db->GetRowsAsoc($sql);
				$mesi = array(
					1 => "GEN",
					2 => "FEB",
					3 => "MAR",
					4 => "APR",
					5 => "MAG",
					6 => "GIU",
					7 => "LUG",
					8 => "AGO",
					9 => "SET",
					10 => "OTT",
					11 => "NOV",
					12 => "DIC",
				);
				foreach($EVENTI as $evento)
				{
					echo'<div class="col-lg-3 col-md-4 col-sm-6 mt-4">
							<a href="scheda-evento.php?id=' . $evento["id"] . '"><div class="event-body">
								<h3 class="event-title">' . $evento["nome_breve"] . '</h3>
								<img alt="immagine' . $evento["nome_breve"] . '" class="img-fluid" src="' . $evento["immagine"] . '">
								<div class="row">
									<div class="col-sm-2">
										<p class="event-date">' . $mesi[$evento["mese"]] . '<br>' . $evento["giorno"] . '</p>
									</div>
									<div class="col-sm-10">
										<p class="event-description"><span class="event-author">' . $evento["nome_breve"] . '</span><br>' . $evento["citta"] . '</p>
									</div>
								</div>
							</div></a>
						</div>';
				}
			?>
		</div>
	</div>
</div>