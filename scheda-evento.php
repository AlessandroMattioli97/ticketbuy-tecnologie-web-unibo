<?php 
include("./header.php");
$id_evento = htmlentities($_GET["id"], ENT_QUOTES);
$sql = "SELECT * FROM evento WHERE id = $id_evento;";
$EVENTO = $db->GetRowsAsoc($sql)[0];

$sql = "SELECT * FROM soggetto_evento WHERE id = " . $EVENTO["id_soggetto"] . ";";
$SOGGETTO = $db->GetRowsAsoc($sql)[0];

$sql = "SELECT id, MONTH(data) as mese, DAY(data) as giorno, YEAR(data) as anno, posti_rimanenti, prezzo, citta FROM data_evento WHERE id_evento = $id_evento and posti_rimanenti > 0;";
$DATE_EVENTO = $db->GetRowsAsoc($sql);

?>
<div class="container-fluid-scheda-evento">
	<?php echo $_SESSION["stato_operazione"]; ?>
	<h2 class="scheda-title"><?php echo $EVENTO["nome_evento"]; ?></h2>
	<div class="row">
		<div class="col-lg-3 col-md-5 col-sm-12">
			<img alt="immagine <?php echo $EVENTO["nome_evento"]; ?>" src="<?php echo $EVENTO["immagine"]; ?>" class="img-fluid mx-auto d-block">
		</div>
		<div class="col-lg-9 col-md-7 col-sm-12 scheda-description">
			<p><?php echo $SOGGETTO["descrizione"]; ?></p>
		</div>
	</div>
	<div class="table-responsive table-info table-responsive-lg scheda-date-evento">
		<table summary="Tabella il cui contenuto descrive un evento e la possibilità di acquistarne i biglietti" class="table">
			<thead>
				<tr align="center">
					<th></th>
					<th id="c1" scope="col">Citta</th>
					<th id="c2" scope="col">Prezzo</th>
					<th id="c3" scope="col">Quantita</th>
					<th id="c4" scope="col">Acquista</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$mesi = array(
						1 => "GEN",
						2 => "FEB",
						3 => "MAR",
						4 => "APR",
						5 => "MAG",
						6 => "GIU",
						7 => "LUG",
						8 => "AGO",
						9 => "SET",
						10 => "OTT",
						11 => "NOV",
						12 => "DIC",
					);
					foreach($DATE_EVENTO as $data)
					{
						if(isset($_SESSION["id_cliente"]))
						{
							echo'<tr>
								<td scope="row"><p class="scheda-event-date">' . $mesi[$data["mese"]] . '<br>' . $data["giorno"] . '<br>' . $data["anno"] . '</p></td>
								<td headers="c1"><p class="scheda-event-description">' . $data["citta"] . '</p></td>
								<td headers="c2"><p class="scheda-event-price">' . $data["prezzo"] . '€</p></td>
								<td headers="c3"><button data-id="' . $data["id"] . '" class="minus button-quantita glyphpro glyphpro-circle_minus scheda-event-icon"></button><p id="q-' . $data["id"] . '" class="quantita" id="quantita">1</p><button data-id="' . $data["id"] . '" class="plus button-quantita glyphpro glyphpro-circle_plus scheda-event-icon"></button></td>
								<td headers="c4"><a id="link-' . $data["id"] . '" href="add-to-cart.php?id=' . $data["id"] . '&id_evento=' . $id_evento . '&quantita=1"><span class="glyphpro glyphpro-cart_in scheda-event-icon"></span></a></td>
							</tr>';
						}
						else
						{
							echo'<tr>
								<td scope="row"><p class="scheda-event-date">' . $mesi[$data["mese"]] . '<br>' . $data["giorno"] . '<br>' . $data["anno"] . '</p></td>
								<td headers="c1"><p class="scheda-event-description">' . $data["citta"] . '</p></td>
								<td headers="c2"><p class="scheda-event-price">' . $data["prezzo"] . '€</p></td>
								<td headers="c3"><button data-id="' . $data["id"] . '" class="minus button-quantita glyphpro glyphpro-circle_minus scheda-event-icon"></button><p id="q-' . $data["id"] . '" class="quantita" id="quantita">1</p><button data-id="' . $data["id"] . '" class="plus button-quantita glyphpro glyphpro-circle_plus scheda-event-icon"></button></td>
								<td headers="c4"><a href="login.php?root=scheda-evento&id=' . $id_evento . '"><span class="glyphpro glyphpro-cart_in scheda-event-icon"></span></a></td>
							</tr>';
						}
						
					}
				?>
			</tbody>
		</table>
	</div>
	<?php echo $_SESSION["stato_operazione"]; ?>
</div>
<?php 
include("./footer.php");
$_SESSION["stato_operazione"] = "";
?>