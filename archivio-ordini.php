<?php
    include("./header.php");
    require_once ('db.class.php');
	include('config_connection.php');
    $myid=$_SESSION["id_cliente"];
    $NumeroRighe = 0;
    $sql="SELECT id_cliente FROM ordini WHERE id_cliente='$myid';";
    $NumeroRighe = $db->NumRows($sql);
?>

<div class="container py-3">
            <h3 class="subtitle">RIEPILOGO DEI MIEI ORDINI</h3>	
            <p class="text-right pt-2">Hai acquistato in tutto <?php echo $NumeroRighe; if($NumeroRighe == 1){echo " ordine";}else{echo " ordini";} ?></p>

        <table class="table table-hover table-dark" summary="tabella che contiene lo storico dei tuoi ordini">
            <thead>
                <tr>
                    <th id="c1" scope="col">Articolo</th>
                    <th id="c2" scope="col">Quantità</th>
                    <th id="c3" scope="col">Prezzo Totale</th>
                    <th id="c4" scope="col">Data Ordine</th>
                </tr>
            </thead>
            <tbody>
                <?php 

                    $sql="SELECT o.data_ordine, o.quantita, o.prezzo_totale, e.nome_evento FROM ((data_evento as de inner join ordini as o on o.id_data_evento = de.id) inner join evento as e on e.id = de.id_evento) WHERE o.id_cliente = '$myid';";
                    $ORDINI = $db->GetRowsAsoc($sql);

                    foreach($ORDINI as $ordine)
					{
                        echo'<tr>';
                            echo'<td class="vertical-center" scope="row" headers="c1">' . $ordine["nome_evento"] . '</td>';
                            echo'<td class="vertical-center" headers="c2">' . $ordine["quantita"] . '</td>';
                            echo'<td class="vertical-center" headers="c3">' . $ordine["prezzo_totale"] . ' €</td>';
                            echo'<td class="vertical-center" headers="c4">' . $ordine["data_ordine"] . '</td>';
                        echo"</tr>";
                        $subtotale = $subtotale + $ordine["prezzo_totale"];		
                    }
                    
                    echo'<tr>';
                        echo '<td class="vertical-center"></td>';
                        echo'<td colspan="4" class="pr-5 vertical-center" headers="c3">Subtotale: ' . $subtotale . ' €</td>';
                    echo'</tr>';
                ?>
            </tbody>
        </table>
</div>

 <?php include("./footer.php") ?>