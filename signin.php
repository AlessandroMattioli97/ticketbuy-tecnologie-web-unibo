<?php
    session_start();
    require_once ('db.class.php');
    include('config_connection.php');
    if((isset($_POST["nome"])) && $_POST["nome"] != "")
    {
        $nome = htmlentities($_POST["nome"], ENT_QUOTES);
        $cognome = htmlentities($_POST["cognome"], ENT_QUOTES);
        $email = htmlentities($_POST["email"], ENT_QUOTES);
        $password = md5(htmlentities($_POST["password"], ENT_QUOTES));
        $cellulare = htmlentities($_POST["cellulare"], ENT_QUOTES);
        $data_nascita = htmlentities($_POST["data_nascita"], ENT_QUOTES);
        $sql = "INSERT INTO clienti(`nome`,`cognome`,`email`,`password`,`cellulare`,`data_nascita`) values ('$nome', '$cognome', '$email', '$password', '$cellulare', '$data_nascita');";
        $result = $db->Query($sql);
        if($result == 1)
        {
            $sql = "SELECT max(id) as id from clienti;";
            $_SESSION["id_cliente"] = $db->GetRowsAsoc($sql)[0]["id"];
            $_SESSION["cliente"] = $nome . " " . $cognome;
            header("location:index.php");
        }
        else
        {
            $_SESSION["stato_operazione"] = '<p class="errore">Inserimento Utente non avvenuto<br>Provare nuovamente.</p>';
        }
    }
?>
<html lang="it">
	<head>
		<link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,700&display=swap" rel="stylesheet">
		<script src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/min.jcarousel.js"></script>
		<script src="./js/bootstrap.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet"  href="css/bootstrap.min.css">
		<meta charset="UTF-8">
		<title>Login | OneTicket</title>
		<style>
			body{background-image: url(img/background.jpg);}
			.mask{background-color: #fff; max-width: 750px; padding: 80px; padding-bottom: 20px; padding-top: 0px; margin: 0px auto; margin-top: 18%; border: 4px solid #d66c0f; border-radius: 20px;}
			.login-btn{ background-color: rgb(0, 82, 204); color: #fff; height: 60px;}
			.logo{ max-width: 200px;}
			.signup{ font-size: 18px; text-align: center; padding-top: 15px;}
			a:hover{ text-decoration: none;}
			.background{background-color:rgba(214, 108, 15, 0.5); height: 100%; position: absolute; width: 100%;}
		</style>
	</head>
	<body>
		<div class="background">
			<div class="container">
				<div class="mask">
					<a href="index.php"><img src="img/logo.png" alt="ticket-buy logo" class="img-fluid logo mx-auto d-block my-3 pt-3"></a>
					<form method="post" action="#">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name"><strong>Nome *</strong></label>
                                <input class="form-control" type="text" name="nome" id="name" placeholder="Inserisci il tuo nome..." required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cognome"><strong>Cognome *</strong></label>
                                <input class="form-control" type="text" name="cognome" id="cognome" placeholder="Inserisci il tuo cognome..." required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
							    <label for="data_nasdcita"><strong>Data di nascita *</strong></label>
							    <input class="form-control" type="date" name="data_nascita" id="data_nascita" placeholder="Inserisci la tua data di nascita..." required>
						    </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cellulare"><strong>Cellulare</strong></label>
                                <input class="form-control" type="text" name="cellulare" id="cellulare" placeholder="Inserisci il tuo cellulare...">
                            </div>
                        </div>
                    </div>
						<div class="form-group">
							<label for="email"><strong>Email *</strong></label>
							<input class="form-control" type="text" name="email" id="email" placeholder="Inserisci la tua email..." required>
						</div>
						<div class="form-group">
							<label for="password"><strong>Password *</strong></label>
							<input class="form-control" type="password" name="password" id="password" placeholder="Inserisci la tua email..." required>
						</div>
						<div class="form-group">
							<input class="form-control login-btn" type="submit" value="SIGN IN">
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>