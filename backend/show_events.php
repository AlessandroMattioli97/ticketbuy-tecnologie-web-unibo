<?php include("./header.php") ?>
<div class="container-fluid">
	<p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-user_group"></span> Gestione Evento >></p>
</div>
<div class="container-fluid">
	<div class="padding">
		<table  class="table table-striped table-responsive table-hover table-lg" summary="tabella che mostra tutti gli eventi">
		<thead>
			<tr align="center">
				<th colspan="7" bgcolor="#71beda">Eventi</th>
			</tr>
			<tr>
                <th id="c1" scope="col">Mod.</th>
                <th id="c2" scope="col">Elim.</th>
				<th id="c3" scope="col">Date</th>
                <th id="c4" scope="col">NOME EVENTO</th>
				<th id="c5" scope="col">SOGGETTO</th>
				<th id="c6" scope="col">TIPOLOGIA EVENTO</th>
				<th id="c7" scope="col">FOTO</th>
			</tr>
		</thead>
		<?php
					$sql = "SELECT e.id as id_evento, e.immagine as immagine_artista, e.*, se.* FROM evento as e inner join soggetto_evento as se on e.id_soggetto = se.id;";
					$EVENTI = $db->GetRowsAsoc($sql);

					foreach($EVENTI as $evento)
					{
						echo'<tr>';
							echo'<td  class="vertical-center" scope="row" headers="c1"><a class="black" href="manage_events.php?id=' . $evento["id_evento"] . '"><span class="glyphpro glyphpro-pencil"></span></a></td>';
							echo'<td class="vertical-center" headers="c2"><a onclick="return(confirm(\'Vuoi Cancellare il record?\'))" class="black" href="delete_record.php?delete=' . $evento["id_evento"] . '&page=show_events"><span class="glyphpro glyphpro-bin"></span></a></td>';
                            echo'<td  class="vertical-center" headers="c3"><a class="black" href="show_date_event.php?id=' . $evento["id_evento"] . '"><span class="glyphpro glyphpro-eye_open"></span></a></td>';
                            echo'<td class="vertical-center" headers="c4">' . $evento["nome_evento"] . '</td>';
							echo'<td class="vertical-center" headers="c5">' . $evento["nome_completo"] . '</td>';
                            echo'<td class="vertical-center" headers="c6">' . $evento["tipologia_evento"] . '</td>';
							echo'<td><img src=".' . $evento["immagine_artista"] . '" class="img-profile" alt="immagine del profilo"  headers="c7"></td>';
						echo"</tr>";		
					}
			?>
		</table>
		<p class="text-center"><?php echo $_SESSION["stato_operazione"]; ?></p>
	</div>
	<a class="wrap-button-attivita-rapida__btn" href="add_events.php"><span class="glyphpro glyphpro-plus black"></span> Aggiungi Evento</a>
</div>
<?php 
include("./footer.php");
$_SESSION["stato_operazione"] = "";

?>