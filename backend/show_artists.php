<?php include("./header.php") ?>
<div class="container-fluid">
	<p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-user_group"></span> Gestione Artista >></p>
</div>
<div class="container-fluid">
	<div class="padding">
		<table  class="table table-striped table-responsive table-hover table-lg" summary="tabella che mostra l'elenco degli artisti registrati al sito">
		<thead>
			<tr align="center">
				<th colspan="7" bgcolor="#71beda">Artista</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
				<th id="c1" scope="col">ID</th>
				<th id="c2" scope="col">NOME COMPLETO</th>
                <th id="c3" scope="col">NOME BREVE</th>
                <th id="c4" scope="col">DESCRIZIONE</th>
                <th id="c5" scope="col">IMMAGINE</th>
			</tr>
		</thead>
		<?php
					$sql = "SELECT * FROM soggetto_evento";
					$ARTISTI = $db->GetRowsAsoc($sql);

					foreach($ARTISTI as $artista)
					{
						echo'<tr>';
							echo'<td  class="vertical-center" scope="row"><a class="black" href="manage_artists.php?id=' . $artista["id"] . '"><span class="glyphpro glyphpro-pencil"></span></a></td>';
							echo'<td  class="vertical-center"><a'; ?> onclick="return(confirm('Vuoi Cancellare il record?'))"  <?php echo' class="black" href="delete_record.php?delete=' . $artista["id"] . '&page=show_artists"><span class="glyphpro glyphpro-bin"></span></a></td>';
							echo'<td class="vertical-center" headers="c1">' . $artista["id"] . '</td>';
                            echo'<td class="vertical-center" headers="c2">' . $artista["nome_completo"] . '</td>';
                            echo'<td class="vertical-center" headers="c3">' . $artista["nome_breve"] . '</td>';
                            echo'<td class="vertical-center" headers="c4">' . $artista["descrizione"] . '</td>';
                            echo'<td headers="c5"><img src="' . $artista["immagine"] . '" class="img-profile alt="immagine artista""></td>';
						echo"</tr>";		
					}
			?>
		</table>
		<p class="text-center"><?php echo $_SESSION["stato_operazione"]; ?></p>
	</div>
	<a class="wrap-button-attivita-rapida__btn" href="add_artists.php"><span class="glyphpro glyphpro-plus black"></span> Aggiungi Artista</a>
</div>
<?php 
	$_SESSION["stato_operazione"] = "";
	include("./footer.php");
?>