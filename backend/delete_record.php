<?php
session_start();
//Sopprimo i warning php
error_reporting(E_ERROR | E_PARSE);
include("./auth.php");
if(isset($_SESSION['email']) && isset($_SESSION['password']) && $autorizzazione == 1)
{
	include 'header.php';
	include 'auth.php';
	if($_GET!=null)
	{
		$sql = "";
		$page = "";
		if($_GET["page"] == "show_users" && $amministratore == 1)
		{
			$page = "admin";
		}
		if($_GET["page"] == "show_events")
		{
			$page = "evento";
		}
		if($_GET["page"] == "show_date_event")
		{
			$page = "data_evento";
		}
		$delete = $_GET["delete"];
		if($page == "admin" && $amministratore == 1)
		{
			$sql = "DELETE FROM $page WHERE id='$delete';";
			$result = $db->Query($sql);
			echo "<script type='text/javascript'>  window.location='show_users.php'; </script>";
		}
		if($page == "evento")
		{
			$sql = "SELECT id from data_evento WHERE id_evento = $delete;";
			$DATE_EVENTI = $db->GetRowsAsoc($sql);
			foreach($DATE_EVENTI as $data)
			{
				$sql = "SELECT c.id as id, o.id as id_ordine FROM clienti as c INNER JOIN ordini as o on c.id = o.id_cliente WHERE o.id_data_evento = " . $data["id"] . ";";
				$CLIENTI_CON_ACQUISTO = $db->GetRowsAsoc($sql);
				foreach($CLIENTI_CON_ACQUISTO as $cliente)
				{
					$sql = "INSERT INTO notifiche(`data`, `descrizione`, `letto`, `cod_cliente`, `cod_data_evento`) VALUES ('" . date("y-m-d") . "', 'La data dell evento è stata ANNULLATA per annullamento del evento', '0', '" . $cliente["id"] . "', '" . $data["id"] . "');";
					$db->Query($sql);
				}
				$sql = "UPDATE ordini SET `stato` = 'DATA EVENTO ANNULLATA' WHERE id = " . $cliente["id_ordine"] . ";";
				$db->Query($sql);

				$sql = "SELECT c.id as id, cr.id as id_carrello FROM clienti as c INNER JOIN carrello as cr on c.id = cr.id_cliente WHERE cr.id_data_evento = " . $data["id"] . ";";
				$CLIENTI_CARRELLO = $db->GetRowsAsoc($sql);
				foreach($CLIENTI_CARRELLO as $cliente)
				{
					$sql = "INSERT INTO notifiche(`data`, `descrizione`, `letto`, `cod_cliente`, `cod_data_evento`) VALUES ('" . date("y-m-d") . "', 'La data dell evento è stata ANNULLATA per annullamento del evento', '0', '" . $cliente["id"] . "', '" . $data["id"] . "');";
					$db->Query($sql);
				}
				$sql = "DELETE FROM carrello WHERE id_data_evento = " . $data["id"] . ";";
				$db->Query($sql);
			}
			
			$sql = "DELETE FROM $page WHERE id='$delete';";
			$result = $db->Query($sql);
			
			$sql = "DELETE FROM data_evento WHERE id_evento='$delete';";
			$result = $db->Query($sql);
			
			echo "<script type='text/javascript'>  window.location='show_events.php'; </script>";
		}
		if($page == "data_evento")
		{
			$sql = "DELETE FROM $page WHERE id='$delete';";
			$result = $db->Query($sql);
			
			$sql = "SELECT c.id as id FROM clienti as c INNER JOIN ordini as o on c.id = o.id_cliente WHERE o.id_data_evento = $delete;";
			$CLIENTI_CON_ACQUISTO = $db->GetRowsAsoc($sql);
			foreach($CLIENTI_CON_ACQUISTO as $cliente)
			{
				$sql = "INSERT INTO notifiche(`data`, `descrizione`, `letto`, `cod_cliente`, `cod_data_evento`) VALUES ('" . date("y-m-d") . "', 'La data dell evento è stata ANNULLATA', '0', '" . $cliente["id"] . "', '$delete');";
				$db->Query($sql);
			}


			$sql = "SELECT c.id as id FROM clienti as c INNER JOIN carrello as cr on c.id = cr.id_cliente WHERE cr.id_data_evento = $delete;";
			$CLIENTI_CARRELLO = $db->GetRowsAsoc($sql);
			foreach($CLIENTI_CARRELLO as $cliente)
			{
				$sql = "INSERT INTO notifiche(`data`, `descrizione`, `letto`, `cod_cliente`, `cod_data_evento`) VALUES ('" . date("y-m-d") . "', 'La data dell evento è stata ANNULLATA', '0', '" . $cliente["id"] . "', '$delete');";
				$db->Query($sql);
			}
			$sql = "DELETE FROM carrello WHERE id_data_evento = $delete;";
			$db->Query($sql);
			
			
			echo "<script type='text/javascript'>  window.location='show_events.php'; </script>";
		}
	}
	include 'footer.php';
}
else if(isset($_SESSION["id_cliente"]))
{
	require_once ('db.class.php');
	include('config_connection.php');
	$sql = "";
	$page = "";
	if($_GET["page"] == "cart")
	{
		$page = "carrello";
	}
	$delete = $_GET["delete"];
	if($page == "carrello")
	{
		$sql = "DELETE FROM $page WHERE id='$delete';";
		$result = $db->Query($sql);
		echo "<script type='text/javascript'>  window.location='../cart.php'; </script>";
	}
}
echo "<script type='text/javascript'>  window.location='./dashboard.php'; </script>";
?>