<?php
include("./header.php");

include("./auth.php");
if($autorizzazione == 1)
{
	include("./resize_and_crop.php");

	if (isset($_POST["nome"])) {
		if (isset($_FILES['foto'])) {
			include("uploaded_photo_settings.php");
			include("uploaded_carousel_settings.php");

			//copio il file dalla sua posizione temporanea alla mia cartella upload
			if (move_uploaded_file($userfile_tmp, $uploaddir . $userfile_name) && move_uploaded_file($userfile_tmp_carousel, $uploaddir_carousel . $userfile_name_carousel)) {
				$nome_evento = htmlentities($_POST["nome"], ENT_QUOTES);
				$artista     = htmlentities($_POST["artista"], ENT_QUOTES);
				$tipologia   = htmlentities($_POST["tipologia"], ENT_QUOTES);
				$foto = htmlentities($uploaddir . $userfile_name, ENT_QUOTES);
				 resize_crop_image(504, 315, $foto, $foto);
				$foto = htmlentities("./img/" . $userfile_name, ENT_QUOTES);
				$carousel = htmlentities($uploaddir_carousel . $userfile_name_carousel, ENT_QUOTES);
				resize_crop_image(2273, 787, $carousel, $carousel);
				$carousel = htmlentities("./img/carousel/" . $userfile_name_carousel, ENT_QUOTES);
				$sql = "INSERT INTO evento(`nome_evento`,`id_soggetto`,`tipologia_evento`,`immagine`,`carousel`) values ('$nome_evento', '$artista', '$tipologia', '$foto', '$carousel');";
				echo $sql;
				$result = $db->Query($sql);
				if ($result == 1) {
					$_SESSION["stato_operazione"] = '<p class="successo">Inserimento Evento avvenuto con successo!!</p>';
					echo "<script type='text/javascript'>  window.location='show_events.php'; </script>";
				} else {
					$_SESSION["stato_operazione"] = '<p class="errore">Inserimento Evento non avvenuto<br>Provare nuovamente.</p>';
					echo "<script type='text/javascript'>  window.location='show_events.php'; </script>";
				}
			}
		}
	}
	?>
<div class="container-fluid">
    <p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-plus"></span> Aggiungi Evento >></p>
</div>
<div class="container-fluid">
    <div class="padding">
        <form enctype="multipart/form-data" class="form-control" method="post" action="#">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="NE">Nome Evento*</label>
                        <input type="text" class="form-control" name="nome" id="NE" required/>
                    </div>
                    <div class="form-group">
                        <label for="SA">Seleziona Artista*</label>
                        <select class="form-control" name="artista" id="SA">
                        <?php
                            $sql = "SELECT * FROM `soggetto_evento`;";
                            $ARTISTI = $db->GetRowsAsoc($sql);
                            foreach($ARTISTI as $artista)
                            {
                            	echo'<option value="' . $artista["id"]  . '">' . $artista["nome_completo"] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
					<div class="form-group">
                        <label for="TE">Tipologia Evento *</label>
                        <select name="tipologia" class="form-control">
                            <option value="Musica" id="TE">Musica</option>
                            <option value="Arte" id="TE">Arte</option>
                            <option value="Teatro" id="TE">Teatro</option>
                            <option value="Festival" id="TE">Festival</option>
                            <option value="Tempo Libero" id="TE">Tempo Libero</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="IE">Immagine Carousel Evento *</label><br>
                        <label class="file custom-file">
                        <input type="file" id="Foto" name="carousel" class="carousel-file-input" id="IE" required>
                        <span class="file-custom custom-file-control" data-content="Choose file..."></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="IE">Immagine Evento *</label><br>
                        <label class="file custom-file">
                        <input type="file" id="Foto" name="foto" class="custom-file-input" id="IE" required>
                        <span class="file-custom custom-file-control" data-content="Choose file..."></span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="AE">Aggiungi evento!</label>
                                <input type="submit" class="form-control color6" id="AE"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="annulla">Annulla</label>
                                <a href="show_events.php"><input type="button" class="form-control color6" value="Annulla" id="annulla"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $("input[type=file]").change(function () {
     var fieldVal = $(this).val();
     if (fieldVal != undefined || fieldVal != "") {
       $(this).next(".custom-file-control").attr('data-content', fieldVal);
     }
    });
	
	$("input[type=file]").change(function () {
     var fieldVal = $(this).val();
     if (fieldVal != undefined || fieldVal != "") {
       $(this).next(".carousel-file-control").attr('data-content', fieldVal);
     }
    });
</script>
<?php 
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>