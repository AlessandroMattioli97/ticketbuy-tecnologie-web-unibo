<?php 
include("./header.php");
include("./auth.php");
if($autorizzazione == 1)
{
	$id_soggetto_evento = htmlentities($_GET["id"], ENT_QUOTES);
	include("./resize_and_crop.php");
		if (isset($_POST["nome_completo"])) 
		{
			if (isset($_FILES['foto']) && $_FILES['foto']['name'] != "") 
			{
				include("uploaded_photo_settings.php");

				//copio il file dalla sua posizione temporanea alla mia cartella upload
				if (move_uploaded_file($userfile_tmp, $uploaddir . $userfile_name)) 
				{
					$nome_completo = htmlentities($_POST["nome_completo"], ENT_QUOTES);
					$nome_breve = htmlentities($_POST["nome_breve"], ENT_QUOTES);
					$descrizione = htmlentities($_POST["descrizione"], ENT_QUOTES);
					$foto = htmlentities($uploaddir . $userfile_name, ENT_QUOTES);
					resize_crop_image(504, 315, $foto, $foto);
					$sql = "UPDATE soggetto_evento SET `nome_completo` = '$nome_completo',`nome_breve` = '$nome_breve',`descrizione` = '$descrizione',`immagine` = '$foto' WHERE id = $id_soggetto_evento;";
					$result = $db->Query($sql);
					if ($result == 1) {
						$_SESSION["stato_operazione"] = '<p class="successo">Modifica Artista avvenuto con successo!!</p>';
						echo "<script type='text/javascript'>  window.location='show_artists.php'; </script>";
					} 
					else 
					{
						$_SESSION["stato_operazione"] = '<p class="errore">Modifica Artista non avvenuto<br>Provare nuovamente.</p>';
						echo "<script type='text/javascript'>  window.location='show_artists.php'; </script>";
					}
				}
			}
			else 
			{
			   $nome_completo = htmlentities($_POST["nome_completo"], ENT_QUOTES);
			   $nome_breve = htmlentities($_POST["nome_breve"], ENT_QUOTES);
			   $descrizione = htmlentities($_POST["descrizione"], ENT_QUOTES);
			   $sql = "UPDATE soggetto_evento SET `nome_completo` = '$nome_completo',`nome_breve` = '$nome_breve',`descrizione` = '$descrizione' WHERE id = $id_soggetto_evento;";
				$result = $db->Query($sql);
				if ($result == 1) {
					$_SESSION["stato_operazione"] = '<p class="successo">Modifica Artista avvenuto con successo!!</p>';
					echo "<script type='text/javascript'>  window.location='show_artists.php'; </script>";
				} else {
					$_SESSION["stato_operazione"] = '<p class="errore">Modifica Artista non avvenuto<br>Provare nuovamente.</p>';
					echo "<script type='text/javascript'>  window.location='show_artists.php'; </script>";
				}
			}
		}
	$sql = "SELECT * FROM soggetto_evento WHERE id = $id_soggetto_evento;";
	$ARTISTA = $db->GetRowsAsoc($sql)[0];
	?>
	<div class="container-fluid">
    <p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-pencil"></span> Modifica Artista >></p>
</div>
<div class="container-fluid">
    <div class="padding">
        <form enctype="multipart/form-data" class="form-control" method="post" action="#">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="NC">Nome Completo *</label>
                        <input value="<?php echo $ARTISTA["nome_completo"] ?>" type="text" class="form-control" name="nome_completo" id="NA" required/>
                    </div>
					<div class="form-group">
                        <label for="NB">Nome Breve *</label>
                        <input value="<?php echo $ARTISTA["nome_breve"] ?>" type="text" class="form-control" name="nome_breve" id="NB" required/>
                    </div>
					<div class="form-group">
                        <label for="IA">Immagine Artista</label><br>
                        <label class="file custom-file">
                        <input type="file" id="Foto" name="foto" class="custom-file-input" id="IA">
                        <span class="file-custom custom-file-control" data-content="Choose file..."></span>
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="descr">Descrizione *</label>
                        <textarea style="height: 127px;" class="form-control" name="descrizione" id="descr" required><?php echo $ARTISTA["descrizione"] ?>"</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="MA">Modifica Artista!</label>
                                <input type="submit" class="form-control color6" id="MA"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="annulla">Annulla</label>
                                <a href="show_artists.php"><input type="button" class="form-control color6" value="Annulla" id="annulla"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $("input[type=file]").change(function () {
     var fieldVal = $(this).val();
     if (fieldVal != undefined || fieldVal != "") {
       $(this).next(".custom-file-control").attr('data-content', fieldVal);
     }
    });
</script>
<?php 
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>