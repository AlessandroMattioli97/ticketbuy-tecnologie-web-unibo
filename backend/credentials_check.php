<?php
session_start();
if(isset($_SESSION['email']) && isset($_SESSION['password']))
{
	header("location:dashboard.php");
}
// Includo la classe
require_once('db.class.php');
include('config_connection.php');

$id          = "";
$NumeroRighe = 0;
// username and password sent from form 
$myusername  = htmlentities($_POST['email'], ENT_QUOTES);
$mypassword  = htmlentities($_POST['password'], ENT_QUOTES);

// To protect MySQL injection (more detail about MySQL injection)
$mypassword = md5($mypassword);

//Eseguo la Query
$sql = "select * from admin where email='$myusername' and password='$mypassword'";
$NumeroRighe = $db->NumRows($sql);

// If result matched $myusername and $mypassword, table row must be 1 row
if ($NumeroRighe == 1) 
{
    // Register $myusername, $mypassword and redirect to file "login_success.php"
    $sql                   = "select id, lvl_aut, nome, cognome from admin where email='$myusername' and password='$mypassword' LIMIT 1";
    $Temp                  = $db->GetRowsAsoc($sql)[0];
    $_SESSION['id']        = $Temp["id"];
    $_SESSION["lvl_auth"]  = $Temp["lvl_aut"];
    $_SESSION["operatore"] = $Temp["nome"] . " " . $Temp["cognome"];
    $_SESSION['email']     = $myusername;
    $_SESSION['password']  = $mypassword;
    $db->CloseConnection();
    header("location:dashboard.php");
}
else if ($NumeroRighe != 1) 
{
    $_SESSION['errore_login'] = '<p class="error">Username o password errati</p>';
    $db->CloseConnection();
    header("location:index.php");
}
?>