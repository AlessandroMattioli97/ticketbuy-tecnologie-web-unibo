<?php 
include("./header.php");
include("./auth.php");
if($autorizzazione == 1)
{
?>
<div class="container-fluid">
	<p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-user_group"></span> Gestione Clienti >></p>
</div>
<div class="container-fluid">
	<p class="text-center"><?php echo $succ_op; ?></p>
	<div class="padding">
		<table  class="table table-striped table-responsive table-hover table-lg" summary="tabella che mostra tutti i clienti registrati al sito">
		<thead>
			<tr align="center">
				<th colspan="10" bgcolor="#71beda">Clienti</th>
			</tr>
			<tr>
				<th id="c1" scope="col">ID</th>
				<th id="c2" scope="col">NOME</th>
				<th id="c3" scope="col">COGNOME</th>
				<th id="c4" scope="col">EMAIL</th>
				<th id="c5" scope="col">DATA DI NASCITA</th>
				<th id="c6" scope="col">CELLULARE</th>
				<th id="c7" scope="col">PASSWORD</th>
			</tr>
		</thead>
		<?php
					$sql = "SELECT * FROM clienti";
					$CLIENTI = $db->GetRowsAsoc($sql);

					foreach($CLIENTI as $cliente)
					{
						echo'<tr>';
							echo'<td scope="row" headers="c1">' . $cliente["id"] . '</td>';
							echo'<td headers="c2">' . $cliente["nome"] . '</td>';
							echo'<td headers="c3">' . $cliente["cognome"] . '</td>';
							echo'<td headers="c4"' . $cliente["email"] . '</td>';
                            echo'<td headers="c5">' . $cliente["data_nascita"] . '</td>';
                            echo'<td headers="c6">' . $cliente["cellulare"] . '</td>';
                            echo'<td headers="c7">' . $cliente["password"] . '</td>';
						echo"</tr>";		
					}
			?>
		</table>
		<p class="text-center"><?php echo $succ_op; ?></p>
	</div>
</div>
<?php 
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>