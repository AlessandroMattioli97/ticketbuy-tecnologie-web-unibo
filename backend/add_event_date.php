<?php
include("./header.php");

include("./auth.php");
if($autorizzazione == 1)
{
	$id_evento = htmlentities($_GET["id"], ENT_QUOTES);

	include("./resize_and_crop.php");

	if (isset($_POST["citta"])) {
		$citta = htmlentities($_POST["citta"], ENT_QUOTES);
		$data = htmlentities($_POST["data"], ENT_QUOTES);
		$prezzo = htmlentities($_POST["prezzo"], ENT_QUOTES);
		$posti = htmlentities($_POST["posti"], ENT_QUOTES);
		$sql = "INSERT INTO data_evento(`citta`,`data`,`prezzo`,`posti_totali`,`posti_rimanenti`,`id_evento`) values ('$citta', '$data', '$prezzo', '$posti', '$posti', '$id_evento');";
		$result = $db->Query($sql);
		if ($result == 1) {
		   $_SESSION["stato_operazione"] = '<p class="successo">Inserimento Data Evento avvenuto con successo!!</p>';
		   echo "<script type='text/javascript'>  window.location='show_date_event.php?id=$id_evento'; </script>";
		 } else {
		   $_SESSION["stato_operazione"] = '<p class="errore">Inserimento Data Evento non avvenuto<br>Provare nuovamente.</p>';
		   echo "<script type='text/javascript'>  window.location='show_date_event.php?id=$id_evento'; </script>";
		 }
	}
	?>
<div class="container-fluid">
    <p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-plus"></span> Aggiungi Data Evento >></p>
</div>
<div class="container-fluid">
    <div class="padding">
        <form enctype="multipart/form-data" class="form-control" method="post" action="#">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="CE">Citta Evento (Es. Cesena, Forlì-Cesena, Italy) *</label>
                        <input type="text" class="form-control" name="citta" id="CE" required/>
                    </div>
					<div class="form-group">
                        <label for="DE">data Evento *</label>
                        <input type="date" name="data" class="form-control" id="DE" required>
                    </div>
                </div>
                <div class="col-md-6">
					<div class="form-group">
                        <label for="PE">Prezzo Evento (Es. 125.90) *</label>
                        <input type="text" class="form-control" name="prezzo" id="PE" required/>
                    </div>
                    <div class="form-group">
                        <label for="PTE">Posti Totali Evento *</label>
                        <input type="text" name="posti" class="form-control" id="PTE" required>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ADE">Aggiungi data evento!</label>
                                <input type="submit" class="form-control color6" id="ADE"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="annulla">Annulla</label>
                                <a href="show_date_event.php?id=<?php echo $id_evento; ?>"><input type="button" class="form-control color6" value="Annulla" id="annulla"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php 
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>