<?php		
include("./header.php");

include("./auth.php");
if($autorizzazione == 1)
{
	if(isset($_GET["id"]) && $_GET["id"] != "")
	{
		$id = $_GET["id"];
		if(isset($_POST["Nome"]) && $_POST["Nome"] != "")
		{
			$result2 = 1;
			$nome = htmlentities($_POST["Nome"], ENT_QUOTES);
			$cognome = htmlentities($_POST["Cognome"], ENT_QUOTES);
			$email = htmlentities($_POST["Email"], ENT_QUOTES);
			$auth = htmlentities($_POST["Lvl_Aut"], ENT_QUOTES);
			
			$sql = "UPDATE admin SET `nome` = '$nome',`cognome` = '$cognome',`email` = '$email',`lvl_aut` = '$auth'  WHERE id = $id;";
			$result1 = $db->Query($sql);
			echo $sql . "<br>";
			if(isset($_POST["Password"]) && $_POST["Password"] != "")
			{
				$password = md5(htmlentities($_POST["Password"], ENT_QUOTES));
				$sql = "UPDATE admin SET `password` = '$password' WHERE id = $id;";
				$result2 = $db->Query($sql);
				echo $sql . "<br>";
			}
			if($result1 == 1 && $result2 == 1)
			{
				$_SESSION["stato_operazione"] = '<p class="successo">Modifica utente avvenuta con successo!!</p>';
				echo "<script type='text/javascript'>  window.location='show_users.php'; </script>";
			}
			else
			{
				$_SESSION["stato_operazione"] = '<p class="errore">Modifica utente non avvenuta, controllare i dati inseriti!!</p>';
				echo "<script type='text/javascript'>  window.location='show_users.php'; </script>";
			}
		}
		$sql = "SELECT * FROM admin WHERE id='$id';";
		$result = $db->GetRowsAsoc($sql)[0];
	?>
			<div class="container-fluid">
				<p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-plus"></span> Modifica Utente >></p>
			</div>
			<div class="container-fluid">
				<div class="padding">
					<form method="post" action="manage_user.php?id=<?php echo $id; ?>">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="N">Nome *</label>
									<input type="text" value="<?php echo $result["nome"]; ?>" class="form-control" name="Nome" id="N" required/>
								</div>
								<div class="form-group">
									<label for="C">Cognome *</label>
									<input type="text"  value="<?php echo $result["cognome"]; ?>" class="form-control" name="Cognome" id="C" required/>
								</div>
								<div class="form-group">
									<label for="E">Email *</label>
									<input type="text"  value="<?php echo $result["email"]; ?>" class="form-control" name="Email" id="E" required/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="P">Password *</label>
									<input type="password" class="form-control" name="Password" id="P"/>
								</div>
								<div class="form-group">
									<label for="LA">Livello Autorizzazione *</label>
									<select name="Lvl_Aut" class="form-control" id="LA" required>
										<option value="<?php echo $result["lvl_aut"]; ?>">Invariato</option>
										<option value="1" id="LA">Solo Visualizzazione</option>
										<option value="2" id="LA">Organizzatore</option>
										<option value="3" id="LA">Amministratore</option>
									</select>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label for="annulla">Annulla</label>
											<a href="show_users.php"><input type="button" class="form-control color6" value="Annulla" id="annulla"></a>
										</div>
										<div class="col-md-6">
											<label for="ID">Invia Dati</label>
											<input type="submit" class="form-control color6" value="Aggiorna Dati" id="ID">
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<script type="text/javascript">
				$("input[type=file]").change(function () {
			  var fieldVal = $(this).val();
			  if (fieldVal != undefined || fieldVal != "") {
				$(this).next(".custom-file-control").attr('data-content', fieldVal);
			  }
			});
			</script>
<?php 
}
else
{ ?>
	<script type='text/javascript'>  window.location='show_users.php'; </script>
<?php }
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>