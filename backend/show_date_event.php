<?php 
include("./header.php");
$id_evento = htmlentities($_GET["id"], ENT_QUOTES);
?>
<div class="container-fluid">
	<p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-user_group"></span> Gestione Date Evento >></p>
</div>
<div class="container-fluid">
	<?php echo $_SESSION["stato_operazione"]; ?>
	<div class="padding">
		<table  class="table table-striped table-responsive table-hover table-lg" summary="tabella che mostra tutte le date per i vari eventi registrati sul sito">
		<thead>
			<tr align="center">
				<th colspan="11" bgcolor="#71beda">Date Evento</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
				<th id="c1" scope="col">ID</th>
				<th id="c2" scope="col">DATA</th>
				<th id="c3" scope="col">CITTA'</th>
                <th id="c4" scope="col">PREZZO</th>
                <th id="c5" scope="col">POSTI TOTALI</th>
                <th id="c6" scope="col">POSTI RIMANENTI</th>
			</tr>
		</thead>
		<?php
					$sql = "SELECT * FROM data_evento WHERE id_evento = $id_evento;";
					$DATE_EVENTO = $db->GetRowsAsoc($sql);

					foreach($DATE_EVENTO as $data_evento)
					{
						echo'<tr>';
							echo'<td  class="vertical-center"><a class="black" href="manage_date_event.php?id=' . $data_evento["id"] . '&id_evento=' . $id_evento . '"><span class="glyphpro glyphpro-pencil"></span></a></td>';
							echo'<td  class="vertical-center"><a'; ?> onclick="return(confirm('Vuoi Cancellare il record?'))"  <?php echo' class="black" href="delete_record.php?delete=' . $data_evento["id"] . '&page=show_date_event"><span class="glyphpro glyphpro-bin"></span></a></td>';
							echo'<td class="vertical-center" scope="row" headers="c1">' . $data_evento["id"] . '</td>';
							echo'<td class="vertical-center" headers="c2">' . $data_evento["data"] . '</td>';
                            echo'<td class="vertical-center" headers="c3">' . $data_evento["citta"] . '</td>';
                            echo'<td class="vertical-center" headers="c4">' . $data_evento["prezzo"] . '</td>';
                            echo'<td class="vertical-center" headers="c5">' . $data_evento["posti_totali"] . '</td>';
                            echo'<td class="vertical-center" headers="c6">' . $data_evento["posti_rimanenti"] . '</td>';
						echo"</tr>";		
					}
			?>
		</table>
		<p class="text-center"><?php echo $succ_op; ?></p>
	</div>
	<a class="wrap-button-attivita-rapida__btn" href="add_event_date.php?id=<?php echo $id_evento; ?>"><span class="glyphpro glyphpro-plus black"></span> Aggiungi Data Evento</a></p>
</div>

<?php 
$_SESSION["stato_operazione"] = "";
include("./footer.php");
?>