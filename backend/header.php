<?php
session_start();
if(isset($_SESSION['email']) && isset($_SESSION['password']))
{
	//Sopprimo i warning php
	error_reporting(E_ERROR | E_PARSE);
	// Includo la classe
	require_once ('db.class.php');
	include('config_connection.php');
	// Creo l'oggetto database
	$ip = $_SERVER['REMOTE_ADDR'];
	$id = $_SESSION['id'];
	$query_user = "select nome, cognome from users where id=$id";
	
	$query_immagine = "select immagine from admin where id=$id";
	$img_profilo = $db->GetRowsAsoc($query_immagine)[0]["immagine"];
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Dashboard - TicketBuy S.r.l</title>
        <script src="./js/jquery-3.3.1.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet"  href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/glyphicons.css">
    </head>
    <body>
        <div class="header color2 padding">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="rounded mx-auto d-block logo" alt="ticket buy logo" src="img/logo.png"/>
                    </div>
                    <div class="col-lg-7 header-menu">
                        <nav class="navbar navbar-expand-lg navbar-dark">
                            <p class="navbar-menu-text">MENU</p>
                            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false">
                            <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="navbar-collapse collapse" id="menu" style="">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active">
                                        <p class="margin-left white"><a href="dashboard.php"><span class="glyphpro glyphpro-home"></span> Dashboard</a></p>
                                    </li>
                                    <li class="nav-item">
                                        <p class="margin-left white dropdown-toggle" data-toggle="dropdown"><span class="glyphpro glyphpro-bank"></span> Eventi</p>
                                        <ul class="dropdown-menu">
                                            <li><a href="./show_events.php"><span class="glyphpro glyphpro-eye_open"></span> Vedi Eventi</a></li>
											<li><a href="./add_events.php"><span class="glyphpro glyphpro-plus"></span> Aggiungi Eventi</a></li>
                                            <li><a href="./show_artists.php"><span class="glyphpro glyphpro-eye_open"></span> Vedi Artisti</a></li>
											<li><a href="./add_artists.php"><span class="glyphpro glyphpro-plus"></span> Aggiungi Artista</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <p class="margin-left white dropdown-toggle" data-toggle="dropdown"><span class="glyphpro glyphpro-user"></span> Clienti</p>
                                        <ul class="dropdown-menu">
                                            <li><a href="./show_clients.php"><span class="glyphpro glyphpro-eye_open"></span> Vedi Clienti</a></li>
                                            <li><a href="./show_newsletters.php"><span class="glyphpro glyphpro-eye_open"></span> Vedi Newsletter</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <p class="margin-left white dropdown-toggle" data-toggle="dropdown"><span class="glyphpro glyphpro-user_group"></span> Utenti</p>
                                        <ul class="dropdown-menu">
                                            <li><a href="./show_users.php"><span class="glyphpro glyphpro-eye_open"></span> Vedi Utenti</a></li>
                                            <li><a href="./add_users.php"><span class="glyphpro glyphpro-plus"></span> Aggiungi Utente</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="col-lg-3">
                        <div class="float-right">
                            <table class="table-responsive" summary="tabella che mostra l'account loggato e la possibilità di fare logout">
                                <tr>
                                    <td scope="row">
                                        <p class="padding-left padding-right"><a href="logout.php" class="white"><span class="glyphpro glyphpro-log_out"></span>Logout</a></p>
                                    </td>
									<td><img class="rounded-circle img-profile" alt="visualizza il tuo profilo" src="<?php echo $img_profilo; ?>"</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	}
	else
	{
			header("location:index.php");
	}
	?>