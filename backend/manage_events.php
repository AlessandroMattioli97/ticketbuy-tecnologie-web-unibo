<?php include ("./header.php") ?>
<?php
include("./auth.php");
if($autorizzazione == 1)
{
    $id_evento = htmlentities($_GET["id"], ENT_QUOTES);
    $sql = "SELECT * FROM evento WHERE id = $id_evento;";
    $EVENTO = $db->GetRowsAsoc($sql) [0];
    $id_artista = $EVENTO["id_soggetto"];
    $sql = "SELECT * FROM soggetto_evento WHERE id = $id_artista;";
    $ARTISTA = $db->GetRowsAsoc($sql) [0];
    $sql = "SELECT * FROM soggetto_evento WHERE id != $id_artista;";
    $ARTISTI = $db->GetRowsAsoc($sql) [0];
    
	include("./resize_and_crop.php");

    if (isset($_POST["nome"])) 
	{
        if ((isset($_FILES['foto']) && $_FILES['foto']['name'] != "") || (isset($_FILES['carousel']) && $_FILES['carousel']['name'] != "")) 
		{
			if(isset($_FILES['foto']) && $_FILES['foto']['name'] != "")
			{
				include("uploaded_photo_settings.php");
			}
            if(isset($_FILES['carousel']) && $_FILES['carousel']['name'] != "")
			{
				include("uploaded_carousel_settings.php");
			}
			
            //copio il file dalla sua posizione temporanea alla mia cartella upload
            if (move_uploaded_file($userfile_tmp, $uploaddir . $userfile_name)) 
			{
                $foto = htmlentities($uploaddir . $userfile_name, ENT_QUOTES);
                resize_crop_image(504, 315, $foto, $foto);
				$foto = htmlentities("./img/" . $userfile_name, ENT_QUOTES);
                $sql = "UPDATE evento SET `immagine` = '$foto' WHERE id = $id_evento;";
                $result = $db->Query($sql);
                if ($result == 1) {
                    $_SESSION["stato_operazione"] = '<p class="successo">Inserimento Evento avvenuto con successo!!</p>';
                } 
				else 
				{
                    $_SESSION["stato_operazione"] = '<p class="errore">Inserimento Evento non avvenuto<br>Provare nuovamente.</p>';
                }
            }
			if (move_uploaded_file($userfile_tmp_carousel, $uploaddir_carousel . $userfile_name_carousel)) 
			{
				$carousel = htmlentities($uploaddir_carousel . $userfile_name_carousel, ENT_QUOTES);
				resize_crop_image(2273, 787, $carousel, $carousel);
				$carousel = htmlentities("./img/carousel/" . $userfile_name_carousel, ENT_QUOTES);
                $sql = "UPDATE evento SET `carousel` = '$carousel' WHERE id = $id_evento;";
                $result = $db->Query($sql);
                if ($result == 1) {
                    $_SESSION["stato_operazione"] = '<p class="successo">Modifica Evento avvenuto con successo!!</p>';
                } 
				else 
				{
                    $_SESSION["stato_operazione"] = '<p class="errore">Modifica Evento non avvenuto<br>Provare nuovamente.</p>';
                }
            }
        }
		if(isset($_POST["nome"])) 
		{
            $nome_evento = htmlentities($_POST["nome"], ENT_QUOTES);
            $artista = htmlentities($_POST["artista"], ENT_QUOTES);
            $tipologia = htmlentities($_POST["tipologia"], ENT_QUOTES);
            $sql = "UPDATE evento SET `nome_evento` = '$nome_evento',`id_soggetto` = '$artista',`tipologia_evento` = '$tipologia' WHERE id = $id_evento;";
            $result = $db->Query($sql);
            if ($result == 1) {
                $_SESSION["stato_operazione"] = '<p class="successo">Modifica Evento avvenuto con successo!!</p>';
                echo "<script type='text/javascript'>  window.location='show_events.php'; </script>";
            } else {
                $_SESSION["stato_operazione"] = '<p class="errore">Modifica Evento non avvenuto<br>Provare nuovamente.</p>';
                echo "<script type='text/javascript'>  window.location='show_events.php'; </script>";
            }
        }
    }
    ?>
<div class="container-fluid">
    <p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-pencil"></span> Modifica Evento >></p>
</div>
<div class="container-fluid">
    <div class="padding">
        <form enctype="multipart/form-data" class="form-control" method="post" action="#">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="NE">Nome Evento*</label>
                        <input value="<?php echo $EVENTO["nome_evento"]; ?>" type="text" class="form-control" name="nome" id="NE" required/>
                    </div>
                    <div class="form-group">
                        <label for="SA">Seleziona Artista*</label>
                        <select class="form-control" name="artista" id="SA">
                            <option value="<?php echo $ARTISTA["id"]; ?>" selected><?php echo $ARTISTA["nome_completo"]; ?></option>
                            <?php
                                foreach ($ARTISTI as $artista) {
                                    echo '<option value="' . $artista["id"] . '">' . $artista["nome_completo"] . '</option>';
                                }
                                ?>
                        </select>
                    </div>
					<div class="form-group">
                        <label for="TE">Tipologia Evento *</label>
                        <select name="tipologia" class="form-control" id="TE">
                            <option value="Musica">Musica</option>
                            <option value="Arte">Arte</option>
                            <option value="Teatro">Teatro</option>
                            <option value="Festival">Festival</option>
                            <option value="Tempo Libero">Tempo Libero</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
					<div class="form-group">
                        <label for="IE">Immagine Carousel Evento</label><br>
                        <label class="file custom-file">
                        <input type="file" id="Foto" name="carousel" class="carousel-file-input" id="IE">
                        <span class="file-custom custom-file-control" data-content="Choose file..."></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="IEE">Immagine Evento</label><br>
                        <label class="file custom-file">
                        <input type="file" id="Foto" name="foto" class="custom-file-input" id="IEE">
                        <span class="file-custom custom-file-control" data-content="Choose file..."></span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ME">Modifica evento!</label>
                                <input type="submit" class="form-control color6" id="ME"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="annulla">Annulla</label>
                                <a href="show_events.php"><input type="button" class="form-control color6" value="Annulla" id="annulla"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $("input[type=file]").change(function () {
     var fieldVal = $(this).val();
     if (fieldVal != undefined || fieldVal != "") {
       $(this).next(".custom-file-control").attr('data-content', fieldVal);
     }
    });
	
	$("input[type=file]").change(function () {
     var fieldVal = $(this).val();
     if (fieldVal != undefined || fieldVal != "") {
       $(this).next(".carousel-file-control").attr('data-content', fieldVal);
     }
    });
</script>
<?php 
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>