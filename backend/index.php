<?php
session_start();
if(isset($_SESSION['email']) && isset($_SESSION['password']))
{
	header("location:dashboard.php");
}
//Sopprimo i warning php
error_reporting(E_ERROR | E_PARSE);
?>
<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,700&display=swap" rel="stylesheet">
		<script src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/min.jcarousel.js"></script>
		<script src="./js/bootstrap.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet"  href="css/bootstrap.min.css">
		<meta charset="UTF-8">
		<title>Login | OneTicket Dashboard</title>
		<style>
			body{background-image: url(img/background.jpg);}
			.mask{background-color: #fff; max-width: 750px; padding: 80px; padding-bottom: 20px; padding-top: 0px; margin: 0px auto; margin-top: 18%; border: 4px solid #d66c0f; border-radius: 20px;}
			.login-btn{ background-color: rgb(0, 82, 204); color: #fff; height: 60px;}
			.logo{ max-width: 200px;}
			.signup{ font-size: 18px; text-align: center; padding-top: 15px;}
			a:hover{ text-decoration: none;}
			.background{background-color:rgba(214, 108, 15, 0.5); height: 100%; position: absolute; width: 100%;}
			.error{color: #f00; font-weight: 700; text-align: center; font-size: 22px; margin-bottom: 0px; }
		</style>
	</head>
	<body>
		<div class="background">
			<div class="container">
				<div class="mask">
					<img src="img/logo_login.png" alt="tickey buy logo" class="img-fluid logo mx-auto d-block my-3 pt-3">
					<form method="post" action="./credentials_check.php">
						<div class="form-group">
							<label for="email"><strong>Email</strong></label>
							<input class="form-control" type="text" name="email" id="email" placeholder="Inserisci la tua email..." required>
						</div>
						<div class="form-group">
							<label for="password"><strong>Password</strong></label>
							<input class="form-control" type="password" name="password" id="password" placeholder="Inserisci la tua email..." required>
						</div>
						<div class="form-group">
							<input class="form-control login-btn" type="submit" value="ACCEDI ALLA DASHBOARD">
						</div>
					</form>
					<?php echo $_SESSION['errore_login']; ?>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	$_SESSION['errore_login'] = "";
?>