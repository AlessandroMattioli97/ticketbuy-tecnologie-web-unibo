<?php 
include("./header.php");
include("./auth.php");
if($autorizzazione == 1)
{
?>
<div class="container-fluid">
	<p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-user_group"></span> Gestione Newsletter >></p>
</div>
<div class="container-fluid">
	<p class="text-center"><?php echo $succ_op; ?></p>
	<div class="padding">
		<table  class="table table-striped table-responsive table-hover table-lg" summary="tabella che mostra le email che hanno aderito alla newsletter ">
		<thead>
			<tr align="center">
				<th colspan="8" bgcolor="#71beda">Newsletter</th>
			</tr>
			<tr>
				<th id="c1" scope="col">ID</th>
				<th id="c2" scope="col">DATA ACQUISIZIONE</th>
				<th id="c3" scope="col">EMAIL</th>
			</tr>
		</thead>
		<?php
					$sql = "SELECT * FROM newsletter";
					$NEWSLETTERS = $db->GetRowsAsoc($sql);

					foreach($NEWSLETTERS as $newsletter)
					{
						echo'<tr>';
							echo'<td class="vertical-center" scope="row" headers="c1">' . $newsletter["id"] . '</td>';
							echo'<td class="vertical-center" headers="c2">' . $newsletter["data_acquisizione"] . '</td>';
							echo'<td class="vertical-center" headers="c3">' . $newsletter["email"] . '</td>';
						echo"</tr>";		
					}
			?>
		</table>
		<p class="text-center"><?php echo $succ_op; ?></p>
	</div>
</div>
<?php 
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>