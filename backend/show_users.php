<?php include("./header.php");
include("./auth.php");
if($autorizzazione == 1)
{
?>
<div class="container-fluid">
	<p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-user_group"></span> Gestione Utenti >></p>
</div>
<div class="container-fluid">
	<?php echo $_SESSION["stato_operazione"]; ?>
	<div class="padding">
		<table  class="table table-striped table-responsive table-hover table-lg" summary="tabella che mostra gli utenti registrati al sito">
		<thead>
			<tr align="center">
				<th colspan="8" bgcolor="#71beda">Utenti</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
				<th id="c1" scope="col">ID</th>
				<th id="c2" scope="col">FOTO</th>
				<th id="c3" scope="col">NOME</th>
				<th id="c4" scope="col">COGNOME</th>
				<th id="c5" scope="col">EMAIL</th>
				<th id="c6" scope="col">TIPOLOGIA</th>
			</tr>
		</thead>
		<?php
					$sql = "SELECT * FROM admin";
					$UTENTI = $db->GetRowsAsoc($sql);
					$tipologia = array(
						1 => "Solo Visulizzazione",
						2 => "Organizzatore",
						3 => "Amministratore"
					);
					foreach($UTENTI as $utente)
					{
						echo'<tr>';
							echo'<td  class="vertical-center"><a class="black" href="manage_user.php?id=' . $utente["id"] . '"><span class="glyphpro glyphpro-pencil"></span></a></td>';
							if($_SESSION["id"] != $utente["id"])
							{
								echo'<td  class="vertical-center"><a'; ?> onclick="return(confirm('Vuoi Cancellare il record?'))"  <?php echo' class="black" href="delete_record.php?delete=' . $utente["id"] . '&page=show_users"><span class="glyphpro glyphpro-bin"></span></a></td>';
							}
							else
							{
								echo'<td></td>';
							}
							echo'<td class="vertical-center" scope="row" headers="c1">' . $utente["id"] . '</td>';
							echo'<td><img src="' . $utente["immagine"] . '" class="img-profile rounded-circle" alt="immagine del profilo" headers="c2"></td>';
							echo'<td class="vertical-center" headers="c3">' . $utente["nome"] . '</td>';
							echo'<td class="vertical-center" headers="c4">' . $utente["cognome"] . '</td>';
							echo'<td class="vertical-center" headers="c5">' . $utente["email"] . '</td>';
							echo'<td class="vertical-center" headers="c6">' . $tipologia[$utente["lvl_aut"]] . '</td>';
						echo"</tr>";		
					}
			?>
		</table>
		<p class="text-center"><?php echo $succ_op; ?></p>
	</div>
	<a class="wrap-button-attivita-rapida__btn" href="add_users.php"><span class="glyphpro glyphpro-plus black"></span> Aggiungi Utente</a></p>
</div>

<?php 
$_SESSION["stato_operazione"] = "";
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>