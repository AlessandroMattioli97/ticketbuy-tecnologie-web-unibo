<?php 
include("./header.php");

include("./auth.php");
if($autorizzazione == 1)
{
	include("./resize_and_crop.php");

	if(isset($_POST["nome"]))
	{
		if(isset($_FILES['foto']))
		{	
						include("uploaded_photo_settings.php");

						//copio il file dalla sua posizione temporanea alla mia cartella upload
						if (move_uploaded_file($userfile_tmp, $uploaddir . $userfile_name)) 
						{
							$nome = htmlentities($_POST["nome"], ENT_QUOTES);
							$cognome = htmlentities($_POST["cognome"], ENT_QUOTES);
							$email = htmlentities($_POST["email"], ENT_QUOTES);
							$foto = htmlentities($uploaddir . $userfile_name, ENT_QUOTES);
							resize_crop_image(200, 200, $foto, $foto);
							$password = htmlentities($_POST["password"], ENT_QUOTES);
							$lvl_aut = htmlentities($_POST["lvl_aut"], ENT_QUOTES);
							$sql = "INSERT INTO admin(`nome`,`cognome`,`email`,`password`,`lvl_aut`,`immagine`) values ('$nome', '$cognome', '$email', '$password', '$lvl_aut', '$foto');";
							$result = $db->Query($sql);
							echo $sql;
							if($result == 1)
							{
								$_SESSION["stato_operazione"] = '<p class="successo">Inserimento Utente avvenuto con successo!!</p>';
							}
							else
							{
								$_SESSION["stato_operazione"] = '<p class="errore">Inserimento Utente non avvenuto<br>Provare nuovamente.</p>';
							}
						}
		}
	}
	?>
<div class="container-fluid">
	<p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-plus"></span> Aggiungi Utente >></p>
</div>
<div class="container-fluid">
	<div class="padding">
		<form enctype="multipart/form-data" class="form-control" method="post" action="#">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="N">Nome *</label>
						<input type="text" class="form-control" name="nome" id="N" required/>
					</div>
					<div class="form-group">
						<label for="C">Cognome *</label>
						<input type="text" class="form-control" name="cognome" id="C" required/>
					</div>
					<div class="form-group">
						<label for="E">Email *</label>
						<input type="text" class="form-control" name="email" id="E" required/>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="IP">Immagine Profilo *</label><br>
						<label class="file custom-file">
							<input type="file" id="Foto" name="foto" class="custom-file-input" id="IP" required>
							<span class="file-custom custom-file-control" data-content="Choose file..."></span>
						</label>
					</div>
					<div class="form-group">
						<label for="PA">Password *</label>
						<input type="password" class="form-control" name="password" id="PA" required/>
					</div>
					<div class="form-group">
						<label for="LA">Livello Autorizzazione *</label>
						<select name="lvl_aut" class="form-control" required>
							<option value="1" id="LA">Solo Visualizzazione</option>
							<option value="2" id="LA">Organizzatore</option>
							<option value="3" id="LA">Amministratore</option>
						</select>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="AU">Aggiungi Utente!</label>
								<input type="submit" class="form-control color6" id="AU"/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="annulla">Annulla</label>
								<a href="show_users.php"><input type="button" class="form-control color6" value="Annulla" id="annulla"></a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$("input[type=file]").change(function () {
  var fieldVal = $(this).val();
  if (fieldVal != undefined || fieldVal != "") {
    $(this).next(".custom-file-control").attr('data-content', fieldVal);
  }
});
</script>
<?php 
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>