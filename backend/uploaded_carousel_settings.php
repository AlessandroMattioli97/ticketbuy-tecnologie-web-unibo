<?php
if (!isset($_FILES['carousel']) || !is_uploaded_file($_FILES['carousel']['tmp_name'])) {
    echo '<p class="errore">Non hai inviato nessun Carousel!...<br>Sarai reidirizzato tra 3 secondi.</p>';
    echo '<script> setTimeout(function() { window.location.href = "show_events.php"; }, 3000);</script>';
    exit;
}
//Verifico se è un immagine
$is_img = getimagesize($_FILES['carousel']['tmp_name']);
if (!$is_img) {
    echo '<p class="errore">Puoi inviare solo immagini<br>Sarai reidirizzato tra 3 secondi.</p>';
    echo '<script> setTimeout(function() { window.location.href = "show_events.php"; }, 3000);</script>';
    exit;
}
//Max 2MB
if ($_FILES['carousel']['size'] > 5194304) {
    echo '<p class="errore">Immagine troppo grande!<br>Sarai reidirizzato tra 3 secondi.</p>';
    echo '<script> setTimeout(function() { window.location.href = "show_events.php"; }, 3000);</script>';
    exit;
}
//Verifico estensione File
$ext_ok = array(
    'png',
    'jpg',
    'jpeg',
    'JPG',
    'JPEG',
    'PNG'
);
$temp   = explode('.', $_FILES['carousel']['name']);
$ext    = end($temp);
if (!in_array($ext, $ext_ok)) {
    echo '<p class="errore">Il file ha un estensione non ammessa!<br>Sarai reidirizzato tra 3 secondi.</p>';
    echo '<script> setTimeout(function() { window.location.href = "show_events.php"; }, 3000);</script>';
    exit;
}
//percorso della cartella dove mettere i file caricati dagli utenti
$uploaddir_carousel = '../img/carousel/';

//Recupero il percorso temporaneo del file
$userfile_tmp_carousel = $_FILES['carousel']['tmp_name'];

//recupero il nome originale del file caricato
$_FILES['carousel']['name'] = date("ymdhs") . "." . $ext;
$userfile_name_carousel = $_FILES['carousel']['name'];
?>