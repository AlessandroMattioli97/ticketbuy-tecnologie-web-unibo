<?php
include("./header.php");

include("./auth.php");
if($autorizzazione == 1)
{
	$id_data_evento = htmlentities($_GET["id"], ENT_QUOTES);
	$id_evento = htmlentities($_GET["id_evento"], ENT_QUOTES);

	$sql = "SELECT * FROM data_evento WHERE id = $id_data_evento;";
	$DATA_EVENTO = $db->GetRowsAsoc($sql)[0];

	include("./resize_and_crop.php");

	if (isset($_POST["data"])) {
		$data = htmlentities($_POST["data"], ENT_QUOTES);
		$prezzo = htmlentities($_POST["prezzo"], ENT_QUOTES);
		$sql = "UPDATE data_evento SET `data` = '$data', `prezzo` = '$prezzo' WHERE id = $id_data_evento;";
		$result = $db->Query($sql);
		if ($result == 1) 
		{
			$sql = "SELECT c.id as id FROM clienti as c INNER JOIN ordini as o on c.id = o.id_cliente WHERE o.id_data_evento = $id_data_evento;";
			$CLIENTI_CON_ACQUISTO = $db->GetRowsAsoc($sql);
			foreach($CLIENTI_CON_ACQUISTO as $cliente)
			{
				$sql = "INSERT INTO notifiche(`data`, `descrizione`, `letto`, `cod_cliente`, `cod_data_evento`) VALUES ('" . date("y-m-d") . "', 'La data dell evento è stata modificata da " . $DATA_EVENTO["data"] . " a $data', '0', '" . $cliente["id"] . "', '$id_data_evento');";
				$db->Query($sql);
			}


			$sql = "SELECT c.id as id FROM clienti as c INNER JOIN carrello as cr on c.id = cr.id_cliente WHERE cr.id_data_evento = $id_data_evento;";
			$CLIENTI_CARRELLO = $db->GetRowsAsoc($sql);
			foreach($CLIENTI_CARRELLO as $cliente)
			{
				$sql = "INSERT INTO notifiche(`data`, `descrizione`, `letto`, `cod_cliente`, `cod_data_evento`) VALUES ('" . date("y-m-d") . "', 'La data dell evento è stata modificata da " . $DATA_EVENTO["data"] . " a $data e il prezzo dell evento e passato da ". $DATA_EVENTO["prezzo"] . " a $prezzo', '0', '" . $cliente["id"] . "', '$id_data_evento');";
				$db->Query($sql);
			}


			$_SESSION["stato_operazione"] = '<p class="successo">Aggiornamento Data Evento avvenuto con successo!!</p>';
			echo "<script type='text/javascript'>  window.location='show_date_event.php?id=$id_evento'; </script>";
		} 
		else 
		{
			$_SESSION["stato_operazione"] = '<p class="errore">Aggiornamento Data Evento non avvenuto<br>Provare nuovamente.</p>';
			echo "<script type='text/javascript'>  window.location='show_date_event.php?id=$id_evento'; </script>";
		}
	}

	?>
<div class="container-fluid">
    <p class="padding margin-left-min black title-content"><span class="glyphpro glyphpro-pencil"></span> Modifica Data Evento >></p>
</div>
<div class="container-fluid">
    <div class="padding">
        <form enctype="multipart/form-data" class="form-control" method="post" action="#">
            <div class="row">
                <div class="col-md-6">
					<div class="form-group">
                        <label for="DE">data Evento *</label>
                        <input type="date" name="data" value="<?php echo $DATA_EVENTO["data"] ?>" class="form-control" id="DE" required>
                    </div>
                </div>
                <div class="col-md-6">
					<div class="form-group">
                        <label for="PE">Prezzo Evento (Es. 125.90) *</label>
                        <input type="text" class="form-control" value="<?php echo $DATA_EVENTO["prezzo"] ?>" name="prezzo" id="PE" required/>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ADE">Aggiungi data evento!</label>
                                <input type="submit" class="form-control color6" id="ADE"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="annulla">Annulla</label>
                                <a href="show_date_event.php?id=<?php echo $id_evento; ?>"><input type="button" class="form-control color6" value="Annulla" id="annulla"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php 
}
else
{
	echo $_SESSION["autorizzazione_negata"];
}
	include("./footer.php");
?>