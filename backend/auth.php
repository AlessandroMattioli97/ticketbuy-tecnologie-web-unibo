<?php
$lvl_auth = $_SESSION["lvl_auth"];
$autorizzazione = 0;
$amministratore = 0;
if($_SESSION["lvl_auth"] == 3)
{
	$amministratore = 1;
}
$_SESSION["autorizzazione_negata"] = '<p class="errore">Non hai i permessi per accedere a questa pagina!!</p>';
switch(basename($_SERVER['PHP_SELF']))
{
	case "add_users.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
	case "manage_user.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
		case "add_artists.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 1;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
	case "add_events.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 1;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
		case "manage_artists.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 1;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
	case "manage_date_event.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 1;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
		case "manage_events.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 1;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
		case "delete_record.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 1;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
		case "show_clients.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
		case "show_users.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
		case "show_newsletters.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
		case "add_event_date.php":
		{
			if($lvl_auth==1)
			{
				$autorizzazione = 0;
			}
			else if($lvl_auth==2)
			{
				$autorizzazione = 1;
			}
			else if($lvl_auth==3)
			{
				$autorizzazione = 1;
			}
			break;
		}
}
?>