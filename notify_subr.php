<?php 
	session_start();
    if(isset($_SESSION["not"]) && $_SESSION["not"] == 1)
    {
        ?>
            <style>
                .alert{padding: 20px; background-color: green; color: white;}
                .closebtn{margin-left: 15px; color: white; font-weight: bold; float: right; font-size: 22px; line-height: 20px; cursor: pointer; transition: 0.3s;}
                .closebtn:hover{color: black;}
            </style>

            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Iscrizione alla newsletter avvenuta con successo!</strong>
            </div> 
        <?php
    }else if(isset($_SESSION["not"]) && $_SESSION["not"] == 2)
    {
        ?>
            <style>
                .alert{padding: 20px; background-color: #f44336; color: white;}
                .closebtn{margin-left: 15px; color: white; font-weight: bold; float: right; font-size: 22px; line-height: 20px; cursor: pointer; transition: 0.3s;}
                .closebtn:hover{color: black;}
            </style>

            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Iscrizione alla newsletter fallita!</strong> L'email è già presente nel database.
            </div> 
        <?php
    }else if(isset($_SESSION["not"]) && $_SESSION["not"] == 3)
    {
        ?>
            <style>
                .alert{padding: 20px; background-color: #f44336; color: white;}
                .closebtn{margin-left: 15px; color: white; font-weight: bold; float: right; font-size: 22px; line-height: 20px; cursor: pointer; transition: 0.3s;}
                .closebtn:hover{color: black;}
            </style>

            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Iscrizione alla newsletter fallita! Email non valida.</strong>
            </div> 
        <?php
    }
?>
