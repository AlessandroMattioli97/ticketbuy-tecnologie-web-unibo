<footer class="footer-color">
    <div class="container">
        <div class="row">
            <div class="col-md-5 py-4 pl-5 pr-5 newsletter">
                <h4>Iscriviti alla Newsletter!!</h4>
                <form method="post" action="iscrizione_newsletter.php">
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
                        		<input class="form-control" type="text" placeholder="Inserisci la tua email!" name="newsletter">
                   			</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
                        		<input type="submit" value="Iscrivimi" class="form-control">
                   			</div>
						</div>
					</div>
                </form>
                <p class="privacy-paragraph">Cliccando su “<strong>Iscrivimi</strong>” acconsenti al trattamento sui dati personali (<a href="#"><span class="privacy-span"><strong>Privacy Policy</strong></span></a>)</p>
            </div>
            <div class="col-md-4 py-4 pl-5 pr-5 link-utili">
                <h4>Link Utili</h4>
                <a href="#">
                    <p>Informativa sulla Privacy</p>
                </a>
                <a href="#">
                    <p>Termini e condizioni di vendita</p>
                </a>
                <a href="#">
                    <p>Diritto di recesso</p>
                </a>
                <a href="#">
                    <p>Coockies</p>
                </a>
            </div>
            <div class="col-md-3 py-4 pl-5 pr-5 social">
                <h4>Social</h4>
                <div class="row">
                    <div class="col-xs-4">
                        <img alt="collegamento a instagram" src="img/insta.png">
                    </div>
                    <div class="col-xs-4">
                        <img alt="collegamento a facebook" src="img/fb.png">
                    </div>
                    <div class="col-xs-4">
                        <img alt="collegamento a youtube" src="img/yt.png">
                    </div>
                </div>
            </div>
			<div class="col-sm-12 py-1 pl-5 pr-5">
				<p class="white-color">Ticket Buy S.r.l - Capitale Sociale: 1.000.000 Euro - P.IVA 4120123456789</p>
			</div>
        </div>
    </div>
</footer>
<script src="js/action.js"></script>
</body>
</html>