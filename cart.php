<?php
include("./header.php");
if(isset($_SESSION["id_cliente"]))
{
    $myid=$_SESSION["id_cliente"];
    $NumeroRighe = 0;
    $sql="SELECT id_cliente FROM carrello WHERE id_cliente='$myid';";
    $NumeroRighe = $db->NumRows($sql);
?>
<div class="container py-3">
        <h3 class="subtitle">RIEPILOGO CARRELLO</h3>
        <p class="text-right pt-2">Il tuo carrello contiene <?php echo $NumeroRighe; if($NumeroRighe == 1){echo " prodotto";}else{echo " prodotti";} ?></p>
		<div class="table-responsive">
			<table class="table table-hover table-dark" summary="tabella che descrive il contenuto del tuo carrello, con la possibilità di eliminare gli articoli dalla tabella">
				<thead>
					<tr>
						<th scope="col" id="c1">Elimina</th>
						<th scope="col" id="c2">Articolo</th>
						<th scope="col" id="c3">Data</th>
						<th scope="col" id="c4">Quantità</th>
						<th scope="col" id="c5">Prezzo Unitario</th>
						<th scope="col" id="c6">Totale</th>
					</tr>
				</thead>
				<tbody>
					<?php 

						$sql="SELECT c.id, c.quantita, de.prezzo, de.data, de.citta, e.nome_evento FROM ((data_evento as de inner join carrello as c on c.id_data_evento = de.id) inner join evento as e on e.id = de.id_evento) WHERE c.id_cliente = '$myid';";
						$CARRELLO = $db->GetRowsAsoc($sql);
					
						$sql = "SELECT SUM(de.prezzo * c.quantita) as totale FROM data_evento as de inner join carrello as c on c.id_data_evento = de.id WHERE id_cliente = $myid;";
						$subtotale = $db->GetRowsAsoc($sql)[0]["totale"];

						foreach($CARRELLO as $carrello)
						{
							$totale = $carrello["quantita"] * $carrello["prezzo"];
							echo'<tr>';
								echo'<td headers="c1" scope="row"><a onclick="return(confirm(\'Confermi di cancellare questo articolo?\'))" class="white" href="./backend/delete_record.php?delete=' . $carrello["id"] . '&page=cart"><span class="glyphpro glyphpro-bin"></span></a></td>';
								echo'<td class="vertical-center td-descrizione" headers="c2">' . $carrello["nome_evento"] . ' - ' . $carrello["citta"] . '</td>';
								echo'<td class="vertical-center td-data" headers="c3">' . $carrello["data"] . '</td>';
								echo'<td class="vertical-center"headers="c4">' . $carrello["quantita"] . '</td>';
								echo'<td class="vertical-center td-prezzo" headers="c5">' . $carrello["prezzo"] . ' €</td>';
								echo'<td class="vertical-center td-prezzo" headers="c6">' . $totale . ' €</td>';
							echo"</tr>";		
						}

						echo'<tr>';
							echo'<td headers="c5 c6" colspan="6" class="pr-5 vertical-center text-right"><strong>Subtotale:</strong> ' . $subtotale . ' €</td>';
						echo'</tr>';
					?>
				</tbody>
			</table>
	</div>
	<a class="wrap-button-attivita-rapida__btn mt-3" href="acquisto.php">Vai al checkout</a>
</div>
<div class="clear"></div>
<?php 
include("./ultimi-biglietti.php");
}
else
{
	header("location:login.php?root=carrello");
}
include("./footer.php");
?>