<?php 
include("./header.php");
$id_cliente = $_SESSION["id_cliente"];
?>
    <div class="container-sm pt-5">
		<?php echo $_SESSION["stato_operazione"]; ?>
		<div class="row">
			<div class="col-md-6">
				<a href="archivio-ordini.php"><div class="jumbotron">
				<div class="row">
					<div class="col-md-4">
						<img src="img/archivio_ordini.png">
					</div>
					<div class="col-md-4">
						<h3>ARCHIVIO ORDINI</h3>
						<p>Controlla tutti i tuoi precedenti acquisti!</p>
					</div>
				</div>
				</div></a>
				<a href="cart.php"><div class="jumbotron">
					<div class="row">
						<div class="col-md-4">
							<img src="img/shopping-cart.png">
						</div>
						<div class="col-md-4">
							<h3>CARRELLO</h3>
							<p>Visualizza lo stato del tuo carrello!</p>
						</div>
					</div>
				</div></a>
			</div>
			<div class="col-md-6">
				<a class="ajaxNotifiche" data-toggle="modal" data-target="#myModal" href="ajax.php?clientId=<?php echo $id_cliente; ?>"><div class="jumbotron">
					<div class="row">
						<div class="col-md-4">
							<img src="img/notification.png">
						</div>
						<div class="col-sm-4">
							<h3>NOTIFICHE</h3>
							<p>Dai un'occhiata alle tue notifiche!</p>
						</div>
					</div>
				</div></a>
				<a href="manage_client_data.php"><div class="jumbotron">
					<div class="row">
						<div class="col-md-4">
							<img src="img/miei-dati.png">
						</div>
						<div class="col-md-4">
							<h3>I MIEI DATI</h3>
							<p>Modifica i tuoi dati personali!</p>
						</div>
					</div>
				</div></a>
			</div>
		</div>
    </div>
<div id="myModal" class="modal fade show">
	<div class="modal-dialog">
  		<div class="modal-content">
   			<div class="modal-header">
    			<h4 class="modal-title">Area Notifiche</h4>
   			</div>
   			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-hover table-bordered table-bordered table-dark" summary="tabella che mostra le tue notifiche">
						<thead>
							<tr>
								<th scope="row" id="c1">Descrizione</th>
								<th scope="row" id="c2">Data</th>
								<th scope="row" id="c3">Letta/Segna come letta</th>
							</tr>
						</thead>
						<tbody class="table-body-notification">
						</tbody>
					</table>
				</div>
			</div>
   			<div class="modal-footer">
    			<button type="button" class="btn btn-primary" data-dismiss="modal">Chiudi</button>
   			</div>
 		 </div>
 	</div>
</div>
<?php 
$_SESSION["stato_operazione"] = "";
include("./footer.php");
?>


