<?php 
	include("./header.php");
?>
<main>
	<?php include("./carousel.php") ?>
	<?php include("./eventi-in-tendenza.php") ?>
	<?php include("./informazioni.php") ?>
	<?php include("./ultimi-biglietti.php") ?>
</main>
<?php include("./footer.php") ?>