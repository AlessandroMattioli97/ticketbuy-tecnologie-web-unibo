<?php include("./header.php") ?>
<?php
	$chiaveRicerca = htmlentities($_GET["ricerca"], ENT_QUOTES);
	if(isset($_GET["ricerca"]))
	{
		$ricerca = htmlentities($_GET["ricerca"], ENT_QUOTES);
		$sql = 'SELECT e.id as id, se.nome_breve as nome_breve, MONTH(MIN(de.data)) as mese, DAY(MIN(de.data)) as giorno, de.citta as citta, e.immagine FROM (`evento` as e inner join data_evento as de on e.id = de.id_evento) INNER JOIN soggetto_evento as se on se.id = e.id_soggetto WHERE (se.nome_breve LIKE "%' . $ricerca . '%" or de.citta LIKE "%' . $ricerca . '%") and de.posti_rimanenti > 0 GROUP BY e.id ORDER BY MIN(de.data) ASC;';
		$numero_eventi = $db->NumRows($sql);
	}
?>
<div class="container-fluid">
	<h3 class="text-center pt-4 termine-ricerca">Eventi per termine ricerca "<span><?php echo $chiaveRicerca; ?></span>" - Trovati <span><?php echo $numero_eventi; ?></span> Eventi!</h3>
</div>
<?php include("./caricatore-eventi.php") ?>
<?php include("./ultimi-biglietti.php") ?>
<?php include("./footer.php") ?>