<?php

class DataBase
{
	private $connected = false; // Variabile di controllo che rileva se
								// la connessione al
								// database è attiva oppure no
	private $lastConnection;	// Variabile che identifica l'ultimo
								// collegamento al database
	private $dbhost,			// Indirizzo server MySQL
            $dbuser,			// Nome utente MySQL
            $dbpwd,				// Password MySQL
            $dbname;			// Nome database

 // Costruttore di classe
	public function __construct($host, $user, $pwd, $name)
	{
		$this->dbhost = $host;
		$this->dbuser = $user;
		$this->dbpwd = $pwd;
		$this->dbname = $name;
	}
	//Ritorna l'ultima connessione
	public function getLastConnection()
	{
		return $this->lastConnection;
	}
	
	// Apre la connessione con il database
	public function OpenConnection()
	{
		// Se è già connesso, esco dalla funzione
		if ($this->connected)
			return $this->lastConnection;

		// Effettuo la connessione al database
		$link = mysqli_connect($this->dbhost, $this->dbuser, $this->dbpwd, $this->dbname);

		// Se non ci sono stati errori di connessione:
		if ($link)
		{
			// Attivo la connessione
			$this->connected = true;
			// Memorizzo l'ultima connessione
			$this->lastConnection = $link;
			// Esco dalla funzione
			return $link;
		}
		// Se ci sono stati errori di connessione restituisco FALSE
		return false;
	}

	// Chiude la connessione con il database
	public function CloseConnection($link=null)
	{
		// Se è già disconnesso, esco dalla funzione
		if (!$this->connected)
			return true;

		// Se non viene specificato $link uso l'ultimo collegamento
		if (!$link)
			$link = $this->lastConnection;

		 // Se la disconnessione dal database è avvenuta senza errori:
		if (mysqli_close($link))
		{
			// Disattivo la connessione ed esco dalla funzione
			$this->connected = false;
			return true;
		}
		// Se invece ci sono stati errori esco dalla funzione con FALSE
		return false;
	}

	// Seleziona un database
	public function SelectDatabase($db_name=null, $link=null)
	{
		// Imposto il nome del database di default, se non specificato
		if (!$db_name)
			$db_name = $this->dbname;

		// Imposto la connessione di default, se non viene specificato
		if (!$link)
			$link = $this->lastConnection;
		
		// Seleziono il database. Se non ci sono errori esco dalla funzione
		if (mysqli_select_db($link, $db_name))
			return true;

		// Se invece ci sono errori esco dalla funzione con FALSE
		return false;
	}

	// Esegue una query SQL
	public function Query($query, $db_name=null)
	{
		// Mi connetto al database (se disconnesso)
		$link = $this->OpenConnection();
		if (!$link)
			return false;

		// Seleziono il database
		if (!$this->SelectDatabase($db_name))
			return false;

		// Eseguo la query
		$result = mysqli_query($link, $query);
		if (!$result)
			return false;

		// Mi disconnetto dal database (se connesso)
		if (!$this->CloseConnection())
			return false;

		// Infine, se non ci sono problemi, esco dalla funzione
		// restituendo il risultato della query
		return $result;
	}
	public function MultiQuery($query, $db_name=null)	
	{
		$link = $this->OpenConnection();
		if (!$link)
			return false;

		// Seleziono il database
		if (!$this->SelectDatabase($db_name))
			return false;

		// Eseguo le query
		$result = mysqli_multi_query($link, $query);
		if (!$result)
			return false;

		// Mi disconnetto dal database (se connesso)
		if (!$this->CloseConnection())
			return false;

		// Infine, se non ci sono problemi, esco dalla funzione
		// restituendo il risultato della query
		return $result;
	}
	
	public function Insert($query)
	{
		// Esegue una query
		// Restituisce il numero di righe della query presa in oggetto
		$result = $this->Query($query);
		if($result)
			return 1;
		else
			return 0;
	}

	// Calcola il numero di righe di una determinata query
	public function NumRows($query)
	{
		// Esegue una query
		// Restituisce il numero di righe della query presa in oggetto
		return mysqli_num_rows($this->Query($query));
	}
	
	//Calcola il numero delle colonne di una tabella
	public function NumColumn($query)
	{
		// Esegue una query
		// Restituisce il numero di colonne della query presa in oggetto
		$result = $this->Query($query);
		$num_fields = mysqli_num_fields($result);
		return $num_fields;
	}

	// Ottiene un array di valori di una singola riga (la prima trovata),
	// in base alla query,
	// oppure un singolo valore, specificando $key (indice della riga)
	public function GetRow($query, $key=null)
	{
		// Ottengo i risultati della query
		$result = $this->Query($query);

		// Ottengo l'array della prima riga, dai risultati di prima
		$row = mysqli_fetch_array($result);
	
		// Se è specificato il nome della colonna ($key) restituisco
		// il valore specifico, altrimenti tutta la riga ($row)
		if ($key)
			return $row[$key];
		else
			return $row;
	}
	public function GetRows($query)
	{
		//Ritorno array associativo e numerico
		$array = null;
		$result = $this->Query($query);
		$num_fields = mysqli_num_fields($result);
		$i=0;
		$array;
		while ($row = mysqli_fetch_array($result, MYSQLI_NUM)) 
		{
			for($k=0; $k<$num_fields; $k++)
			{
 				$array[$i][$k]= $row[$k]; 
			}
			$i++;
		}
		return $array;
	}
	
	public function GetRowsAsoc($query)
	{
		//Ritorno array associativo e numerico
		$array = null;
		$result = $this->Query($query);
		$num_fields = mysqli_num_fields($result);
		$i=0;
		$array;
		while ($row = mysqli_fetch_assoc($result)) 
		{
 			$array[$i]= $row; 
			$i++;
		}
		return $array;
	}
}
?>